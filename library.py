# Library view
from PyQt5 import Qt, QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QVariant, Qt, QSortFilterProxyModel, QObject, pyqtSignal
from mpd import (MPDClient, CommandError)
from random import choice
from socket import error as SocketError
from sys import exit
from daemonize import Daemonize
from select import select
from datetime import datetime
import threading
import logging
import time
import os
import signal
import sys
import notify2
import configparser
from mpd_controler import MPDControler, LibraryUpdaterThread
import myPyMPD_library

class myPyMPD_library(QtWidgets.QDialog, myPyMPD_library.Ui_dlgLibrary):
	artistProxy = None
	albumsProxy = None
	tracksProxy = None
	
	signal_addToPlaylist = pyqtSignal(list, name="AddTracksToPlaylist")
	signal_replacePlaylist = pyqtSignal(list, name="replaceCurrPlaylist")
	signal_updateLibrary = pyqtSignal(name="updateLibrary")
	
	def __init__(self, cfg=None):
		super(self.__class__, self).__init__()
		print("[INFO] Initializing library...")
		self.setupUi(self)
		self.progress.setVisible(False)
		self.cfg = cfg
		self.db_path = "%s/database/" % self.cfg['paths']['storage']
		
		self.artistProxy = QtCore.QSortFilterProxyModel()
		self.albumsProxy = QtCore.QSortFilterProxyModel()
		self.tracksProxy = QtCore.QSortFilterProxyModel()
		self.tracksProxy.setFilterKeyColumn(-1)
		
		self.artistProxy.setDynamicSortFilter(True)
		self.albumsProxy.setDynamicSortFilter(True)
		self.tracksProxy.setDynamicSortFilter(True)
		
		# link proxies and tables
		self.listArtists.setModel(self.artistProxy)
		self.listAlbums.setModel(self.albumsProxy)
		self.listTracks.setModel(self.tracksProxy)
		
		# listArtist has to change selection for listAlbums and listTracks
		self.listArtists.clicked.connect(self.clearAlbumSelection)
		self.listArtists.clicked.connect(self.clearTrackSelection)
		# and listAlbums has to change listTracks selection
		self.listAlbums.clicked.connect(self.clearTrackSelection)
		
		# link listArtists and listAlbums click event
		self.listArtists.clicked.connect(self.dbFindTracks)
		self.listAlbums.clicked.connect(self.dbFindTracks)
		
		# connect playlist buttons
		self.btnAddToPlaylist.clicked.connect(self.addToPlaylist)
		self.btnReplacePlaylist.clicked.connect(self.replacePlaylist)
		
		# connect library buttons
		self.btnRescan.clicked.connect(self.updateLibrary)
		self.btnUpdateLocal.clicked.connect(self.generate_local_lib)
		
		# link listTracks and btnAddToPlaylist/btnReplacePlaylist - these should be disabled if no selection is made in tracks table.
		self.listTracks.selectionModel().selectionChanged.connect(self.trackSelChange)
		
		# link filters
		self.inpFilterArtists.textChanged.connect(self.filterArtists)
		self.inpFilterAlbums.textChanged.connect(self.filterAlbums)
		self.inpFilterTracks.textChanged.connect(self.filterTracks)
		
		# now, call list/update and list
		# check does external library exist
		if not self.test_library():
			if not os.path.exists(self.db_path):
				os.mkdir(self.db_path)
			
			print("[Library] No local database found. Generating new, to speed dialog loading up a bit next time...")
			self.generate_local_lib()
		else:
			print("[Library] Local database found, using it for media sources")
			self.read_local_lib()
	
	def addToPlaylist(self):
		trackSelection = self.listTracks.selectionModel()
		
		if trackSelection.hasSelection():
			trackModelIndexList = trackSelection.selectedRows()
			
			tracks = []
			
			for trackModelIndex in trackModelIndexList:
				r = trackModelIndex.row()
				
				t = self.tracksProxy.index(r, 6).data()
			
				tracks.append(t)
		
			self.signal_addToPlaylist.emit(tracks)
		
	def replacePlaylist(self):
		trackSelection = self.listTracks.selectionModel()
		
		if trackSelection.hasSelection():
			trackModelIndexList = trackSelection.selectedRows()
			
			tracks = []
			
			for trackModelIndex in trackModelIndexList:
				r = trackModelIndex.row()
				
				t = self.tracksProxy.index(r, 6).data()
				
				tracks.append(t)
		
			self.signal_replacePlaylist.emit(tracks)
	
	idleStatus = None
	def updateLibrary(self):
		msg = QtWidgets.QMessageBox(self)
		msg.setWindowTitle("Information")
		msg.setText("This may take a while. Please wait and don't close any windows")
		msg.exec_()
		
		self.idleStatus = self.txtStatus.text()
		
		self.showProgress([0, 0], [0, 0], "Updating MPD library...")
			
		c = MPDControler(self.cfg)
		c.updateFinished.connect(self.libraryUpdated)
		c.update()
	
	def libraryUpdated(self, changes):
		msg = QtWidgets.QMessageBox(self)
		
		updated = False
		
		if 'database' in changes:
			msg.setWindowTitle("Question")
			msg.setText("MPD library updated. Update local database, too?")
			msg.addButton(QtWidgets.QMessageBox.Ok)
			msg.addButton(QtWidgets.QMessageBox.Cancel)
			updated = True
		else:
			msg.setWindowTitle("Update finished")
			msg.setText("MPD library update finished, no changes found.")
			msg.addButton(QtWidgets.QMessageBox.Ok)
			self.progress.setVisible(False)
			self.txtStatus.setText(self.idleStatus)
		
		exc = msg.exec_()
		
		if exc == QtWidgets.QMessageBox.Ok and updated:
			self.generate_local_lib()
	
	def trackSelChange(self):
		trackSelection = self.listTracks.selectionModel()
		
		state = trackSelection.hasSelection()

		self.btnAddToPlaylist.setEnabled(state)
		self.btnReplacePlaylist.setEnabled(state)
	
	def clearTrackSelection(self):
		trackSelection = self.listTracks.selectionModel()
		
		if trackSelection.hasSelection():
			self.listTracks.clearSelection()
		
	def clearAlbumSelection(self):
		albumSelection = self.listAlbums.selectionModel()
		
		if albumSelection.hasSelection():
			self.listAlbums.clearSelection()
	
	def filterArtists(self, txt):
		self.artistProxy.setFilterFixedString(txt)
	
	def filterAlbums(self, txt):
		self.albumsProxy.setFilterFixedString(txt)
	
	def filterTracks(self, txt):
		self.tracksProxy.setFilterFixedString(txt)
	
	def test_library(self):
		ok = True
		
		ok = os.path.exists("%s/artists" % self.db_path)
		ok = os.path.exists("%s/albums" % self.db_path)
		ok = os.path.exists("%s/tracklist" % self.db_path)
		
		return ok
			
	def generate_local_lib(self, update=False):
		if update:
			msg = QtWidgets.QMessageBox(self)
			msg.setWindowTitle("Local library")
			msg.setText("Update starting. Don't close this window. App may look unresponsive.")
			msg.exec_()
		
		# disable rescan and update buttons
		self.btnRescan.setEnabled(False)
		self.btnUpdateLocal.setEnabled(False)
		
		self.backgroundThread = LibraryUpdaterThread(self.cfg)
		self.backgroundThread.updateProgress.connect(self.showProgress)
		self.backgroundThread.updateFinished.connect(self.writeLocalDb)
		self.backgroundThread.start()
	
	def read_local_lib(self):
		# enable rescan and update buttons
		self.btnRescan.setEnabled(False)
		self.btnUpdateLocal.setEnabled(False)
		
		self.libraryThread = LibraryLoaderThread(self.db_path)
		self.libraryThread.loadingProgress.connect(self.showProgress)
		self.libraryThread.loadingFinished.connect(self.load_library)
		self.libraryThread.start()
	
	def writeLocalDb(self, artists, albums, tracks):
		import pickle
		
		with open("%s/artists" % self.db_path, 'wb') as art_db:
			pickle.dump(artists, art_db, pickle.HIGHEST_PROTOCOL)
		art_db.close()
		
		with open("%s/albums" % self.db_path, 'wb') as alb_db:
			pickle.dump(albums, alb_db, pickle.HIGHEST_PROTOCOL)
		alb_db.close()		
		
		with open("%s/tracklist" % self.db_path, 'wb') as track_db:
			pickle.dump(tracks, track_db, pickle.HIGHEST_PROTOCOL)
		track_db.close()
		
		# load data
		self.read_local_lib()
	
	def secondsToTime(self, s):
		m, s = divmod(s, 60)
		h, m = divmod(m, 60)
		
		if h > 0:
			t = str("%02d:%02d:%02d" %(h,m, s))
		else:
			t = str("%02d:%02d" %(m, s))
			
		return t

	artist_model = None
	albums_model = None
	tracks_model = None
	
	def load_library(self, artists=None, albums=None, tracks=[]):
		if artists != None:
			len_artists = len(artists)
			if len_artists > 0:
				self.artist_model = QtGui.QStandardItemModel(len_artists, 1)
		if albums != None:
			len_albums = len(albums)
			if len_albums > 0:
				self.albums_model = QtGui.QStandardItemModel(len_albums, 1)
		
		len_tracks = len(tracks)
		self.tracks_model = QtGui.QStandardItemModel(len_tracks, 7)
		
		# hide progress bars
		self.progress.setVisible(False)
		self.txtStatus.setText("Please wait, processing data...")
		
		if artists != None and len(artists) > 0:
			i = 1
			# Add all item
			self.artist_model.setItem(0, 0, QtGui.QStandardItem("All artists"))
			# now add others
			for a in artists:
				artist = "Unknown Artist"
				if a != "" and len(a) > 1:
					artist = a
					
				item = QtGui.QStandardItem(artist)
				self.artist_model.setItem(i, 0, item)
				i+=1
			
			self.artistProxy.setSourceModel(self.artist_model)
		
		if albums != None and len(albums) > 0:
			j = 1
			self.albums_model.setItem(0, 0, QtGui.QStandardItem("All albums"))
			for al in albums:
				album = "Unknown Album"
				if al != "" and len(al) > 1:
					album = al
				
				item1 = QtGui.QStandardItem(album)
				self.albums_model.setItem(j, 0, item1)
				j+=1
			
			self.albumsProxy.setSourceModel(self.albums_model)
		
		# finally, tracks!	
		z = 0
		tracks_header = ["Artist", "Album", "Title", "Track#", "Duration", "Year", "Path"]
		for h in tracks_header:
			self.tracks_model.setHeaderData(z, QtCore.Qt.Horizontal, h)
			z+=1
		
		y = 0
		for t in tracks:
			self.add_row(y, t)
			y+=1
			
		self.tracksProxy.setSourceModel(self.tracks_model)
		
		self.txtStatus.setText("Ready. %d artists, %d albums, %d tracks" % (len_artists, len_albums, len_tracks))
		
		self.listTracks.resizeColumnsToContents()
		
		# enable rescan and update buttons
		self.btnRescan.setEnabled(True)
		self.btnUpdateLocal.setEnabled(True)
	
	def add_row(self, row, row_items):
		y = row
		t = row_items
		try:
			artist = "Unknown Artist"
			album = "Unknown Album"
			title = "Unknown Title"
			year = "N/A"
			track = "N/A"
			
			if 'artist' in t:
				if type(t['artist']) == list:
					artist = t['artist'][0]
				else:
					artist = t['artist']
			if 'album' in t:
				if type(t['album']) == list:
					album = t['album'][0]
				else:
					album = t['album']
			if 'title' in t:
				if type(t['title']) == list:
					title = t['title'][0]
				else:
					title = t['title']
				
			if 'track' in t:
				track = t['track']
			if 'year' in t:
				year = t['year']
			duration = self.secondsToTime(int(t['time']))
			path = t['file']
			
			i_artist = QtGui.QStandardItem(artist)
			i_album = QtGui.QStandardItem(album)
			i_title = QtGui.QStandardItem(title)
			i_track = QtGui.QStandardItem(track)
			i_dur = QtGui.QStandardItem(duration)
			i_year = QtGui.QStandardItem(year)
			i_path = QtGui.QStandardItem(path)
		
			self.tracks_model.setItem(y, 0, i_artist)
			self.tracks_model.setItem(y, 1, i_album)
			self.tracks_model.setItem(y, 2, i_title)
			self.tracks_model.setItem(y, 3, i_track)
			self.tracks_model.setItem(y, 4, i_dur)
			self.tracks_model.setItem(y, 5, i_year)
			self.tracks_model.setItem(y, 6, i_path)
		except:
			print("[ERR] failed adding row!")

	def showProgress(self, a_prog, al_prog, s_prog=None):
		if not self.progress.isVisible():
			self.progress.setVisible(True)
		
		if a_prog[1] > 0 and a_prog[0] > 0:
			fa_prog = (a_prog[1]/a_prog[0])*100
		else:
			fa_prog = 0
		
		if al_prog[0] > 0 and al_prog[1] > 0:
			fal_prog = (al_prog[1]/al_prog[0])*100
		else:
			fal_prog = 0
		
		if s_prog == None:
			s_prog = "Updating library tree... %.1f %%" % fa_prog
		
		self.txtStatus.setText(s_prog)	
	
		if a_prog[1] == 0: self.progress_artist.setMaximum(0)
		else: self.progress_artist.setMaximum(100)
		
		if al_prog[1] == 0: self.progress_album.setMaximum(0)
		else: self.progress_album.setMaximum(100)	
		
		self.progress_artist.setValue(fa_prog)
		self.progress_album.setValue(fal_prog)
		
	
	def dbFindTracks(self):
		artistSel = self.listArtists.selectionModel()
		albumSel = self.listAlbums.selectionModel()
		
		artist_r = 0
		artist = None
		if artistSel.hasSelection():
			ArtistModelIndex = artistSel.selectedRows()[0]
			artist_r = ArtistModelIndex.row()
			if artist_r != 0:
				artist = self.artistProxy.index(artist_r, 0).data()
			
		album_r = 0
		album = None
		if albumSel.hasSelection():
			AlbumModelIndexList = albumSel.selectedRows()
			for AlbumModelIndex in AlbumModelIndexList:
				album_r = AlbumModelIndex.row()
				if album_r != 0:
					if album == None:
						album = []
					album.append(self.albumsProxy.index(album_r, 0).data())
		
		# now, thread it boy!
		if artist_r > 0 or album_r > 0:
			searchThread = TrackListThread(self.db_path, artist, album, self)
			searchThread.searchProgress.connect(self.showProgress)
			searchThread.searchFinished.connect(self.load_library)
			searchThread.start()
		else:
			self.read_local_lib() # todo: store original data models somehow, simply to avoid 30+s wait

class TrackListThread(QtCore.QThread):
	searchProgress = pyqtSignal(list, list, str, name="SearchProgress") 
	searchFinished = pyqtSignal(list, list, list, name="SearchFinished")
	
	def __init__(self, library, artist=None, album=None, parent=None):
		super(TrackListThread, self).__init__(parent)
		self.artist = artist
		self.album = album
		self.db_path = library
		
	def run(self):
		# set flags first.
		artist_set = False
		album_set = False
		if self.artist != None:
			artist_set = True
		if self.album != None:
			album_set = True
		
		import pickle
		with open("%s/tracklist" % self.db_path, 'rb') as track_db:
			tracks = pickle.load(track_db)
		track_db.close()
			
		if artist_set and not album_set:
			
			by_artist = tracks[self.artist]
			albums = list(by_artist.keys())
			album_c = len(albums)
			
			tracklist = []
			a_i = 0
			for album in albums:
				album_tracks = by_artist[album]
				tracks_c =len(album_tracks)
				t_i = 0
				for t in album_tracks:
					tracklist.append(t)
					t_i+=1
					
					self.searchProgress.emit([album_c, a_i], [tracks_c, t_i], "Reading '%s' by %s (%.1f %%)" % (album, self.artist, (t_i/tracks_c)*100))
				a_i+=1		
				self.searchProgress.emit([album_c, a_i], [tracks_c, t_i], "Reading '%s' by %s (%.1f %%)" % (album, self.artist, (t_i/tracks_c)*100))
			
			self.searchFinished.emit([], albums, tracklist)
		
		elif artist_set and album_set:			
			by_artist = tracks[self.artist]
			album_tracks = []
			
			for album in self.album:
				if album in by_artist:
					album_tracks += by_artist[album]
			
			t_c = len(album_tracks)
			t_i = 0
			
			tracklist = []
			
			for t in album_tracks:
				tracklist.append(t)
				t_i+=1
				self.searchProgress.emit([100, 100], [t_c, t_i], "Reading '%s' by %s (%.1f %%)" % (self.album, self.artist, (t_i/t_c)*100))
			
			self.searchFinished.emit([], [], tracklist)
		
		elif not artist_set and album_set:
			tracklist = None
			
			a_c = len(tracks)
			
			a_i = 0
			for a in tracks:
				by_artist = tracks[a]
				
				al_c = len(by_artist)
				al_i = 0
				
				for album in self.album:
					if album in by_artist.keys():
						if tracklist == None:
							tracklist = []
						
						tracklist += by_artist[album]
					self.searchProgress.emit([a_c, a_i], [al_c, al_i], "Searching tracks from album '%s' (%.1f %%)" % (album, (a_i/a_c)*100))
				al_i+=1
				a_i+=1
				
				
			
			self.searchFinished.emit([], [], tracklist)

class LibraryLoaderThread(QtCore.QThread):
	loadingProgress = pyqtSignal(list, list, str, name="LoadProgress") 
	loadingFinished = pyqtSignal(list, list, list, name="LoadFinished")
	
	def __init__(self, library, parent=None):
		super(LibraryLoaderThread, self).__init__(parent)
		self.db_path = library
		
	def run(self):
		data = None
		
		import pickle
			
		with open("%s/artists" % self.db_path, 'rb') as art_db:
			artistdata = pickle.load(art_db)
		art_db.close()
		
		with open("%s/albums" % self.db_path, 'rb') as alb_db:
			albumdata = pickle.load(alb_db)
		alb_db.close()		
		
		with open("%s/tracklist" % self.db_path, 'rb') as track_db:
			tracks = pickle.load(track_db)
		track_db.close()
		
		a_count = len(artistdata)
		#al_count = len(albumdata)
		
		trackdata = []
		
		a_i = 0
		for artist in tracks:
			albums = tracks[artist]
			al_i = 0
			al_count = len(albums)
			for album in albums:
				tracklist = albums[album]
				for t in tracklist:
					trackdata.append(t)
				al_i+= 1
				self.loadingProgress.emit([a_count, a_i], [al_count, al_i], "Loading library (%.1f %%)" % ((a_i/a_count)*100))
			a_i+=1
			self.loadingProgress.emit([a_count, a_i], [al_count, al_i], "Loading library (%.1f %%)" % ((a_i/a_count)*100))
			
		self.loadingFinished.emit(artistdata, albumdata, trackdata)