# settings
from PyQt5 import Qt, QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QVariant, Qt, QSortFilterProxyModel, QObject, pyqtSignal
from mpd import (MPDClient, CommandError)
from random import choice
from socket import error as SocketError
from sys import exit
from daemonize import Daemonize
from select import select
from datetime import datetime
import threading
import logging
import time
import os
import signal
import sys
import notify2
import configparser
from mpd_controler import MPDControler
import myPyMPD_settings

class myPyMPD_settings(QtWidgets.QDialog, myPyMPD_settings.Ui_dlgSettings):
	settingsChanged = pyqtSignal(configparser.ConfigParser, name="SettingsChanged")
	def __init__(self, currentConfig=None):
		super(self.__class__, self).__init__()
		print("[INFO] Initializing settings dialog...")
		self.setupUi(self)
		
		# grab old config from myPyMPD window. Sent as initializer, set as None in case of failure.
		self.oldCfg = currentConfig
		
		# Connect page buttons to pageChanged()
		self.btnMPDSets.clicked.connect(self.pageChanged)
		self.btnScrobblerSets.clicked.connect(self.pageChanged)
		self.btnPlaybackSets.clicked.connect(self.pageChanged)
		self.btnDynamicPls.clicked.connect(self.pageChanged)
		self.btnNotifications.clicked.connect(self.pageChanged)
		
		# connect events from page 1
		self.btnTestConn.clicked.connect(self.test_connection)
		self.btnDefaultsConn.clicked.connect(self.mpd_defaults)
		self.inpConnHost.editingFinished.connect(self.expand_mpd_socket_path)
		self.inpConnHost.editingFinished.connect(self.test_mpd_socket_path)
		self.inpMPDConfigDir.editingFinished.connect(self.generate_user_paths)
		
		# connect buttons from page 2 - TODO
		self.inpLogAfter.valueChanged.connect(self.logAfterUpdate)
		self.inpLogPath.editingFinished.connect(self.expand_log_path)
		self.inpLogFormat.currentTextChanged.connect(self.set_logfile_name)
		
		# connect buttons from page 3
		self.btnTestLastFM.clicked.connect(self.test_lastfm)
		
		# connect save button
		self.buttonBox.button(QtWidgets.QDialogButtonBox.Save).clicked.connect(self.saveConfig)
		# ok, now, LOAD those settings
		self.loadConfig()
		
		""" these have to be triggered AFTER settings are loaded, okay? """
		# always set MPD page as first one for configuration.
		self.pageChanged()		
	
	def test_lastfm(self):
		data = {}
		data['username'] = self.inpLastFMusr.text()
		data['password'] = self.inpLastFMpwd.text()
		data['api'] = self.inpLastFMAPI.text()
		data['secret'] = self.inpLastFMShared.text()
	
		from scrobbler import LastFMScrobbler
		lfm_scrobbler = LastFMScrobbler(self.oldCfg, None, test=True)
		res = lfm_scrobbler.test_data(data)
		
		msg = QtWidgets.QMessageBox(self)
		if not res:	
			msg.setWindowTitle("Error")
			msg.setText("Last.fm login failed. Check your username/pasword combination and make sure you have correct API and secret keys.")
		else:
			msg.setWindowTitle("Success!")
			msg.setText("Successfully logged in as %s" % (data['username']))
		msg.exec_()
		
	def expand_log_path(self):
		path = self.inpLogPath.text()
		if "~" in path:
			print("[DBG] expanding path")
			h_path = os.path.expanduser("~")
			path = path.replace("~", h_path, 1)
			self.inpLogPath.setText(path)
			self.set_logfile_name(self.inpLogFormat.currentText())

	def set_logfile_name(self, l_format):
		ext = None
		
		if l_format == "Text":
			ext = "txt"
		elif l_format == "JSON":
			ext = "json"
		elif l_format == "XML":
			ext = "xml"
		elif l_format == "CSV":
			ext = "csv"
			
		path = self.inpLogPath.text()
		
		if ".txt" in path or ".json" in path or ".xml" in path or ".csv" in path:
			old_ext = os.path.splitext(path)[1]
			path = path.replace(old_ext, ".%s" % ext)
		else:
			path = "%s.%s" % (path, ext)
		
		self.inpLogPath.setText(path)
	
	def test_connection(self):
		host = self.inpConnHost.text()
		port = self.inpConnPort.text()
		auth = self.inpConnAuth.isChecked()
		pswd = self.inpConnPwd.text()
		
		# let's think it will fail, by default.
		t_msg = ["Connection failed", "Cannot connect to provided host. Please check your settings"]
		
		from mpd_controler import MPDControler
		
		c = MPDControler(self.oldCfg)
		
		if auth:
			t_succ = c.test_connection(host, pswd, port)
		else:
			t_succ = c.test_connection(host, port)
	
		if t_succ:
			t_msg[0] = "Connection success!"
			t_msg[1] = "Successfully connected to provided host!"
		
		msg = QtWidgets.QMessageBox(self)
		msg.setWindowTitle(t_msg[0])
		msg.setText(t_msg[1])
		msg.exec_()		
	
	def generate_user_paths(self):
		auto_dirs = False
		home_path = None
		
		mpd_path = self.inpMPDConfigDir.text()
		
		# if user gives home folders as paths for MPD, try searching for music and playlists there.
		if "~" in mpd_path or "home" in mpd_path:
			auto_dirs = True
			# universal way to grab users home, who knows, some people maybe mount their home stuff under /mnt/homes/home1/username
			home_path = os.path.expanduser("~")
			
			mpd_path = mpd_path.replace("~", home_path, 1)
			if os.path.exists(mpd_path):
				self.inpMPDConfigDir.setText(mpd_path)
				
				playlist_path = "%s/playlists" % mpd_path
				music_path = "%s/Music" % home_path
				
				if os.path.exists(playlist_path):
					self.inpMPDPlaylistDir.setText(playlist_path)
				if os.path.exists(music_path):
					self.inpMusicDir.setText(music_path)
			else:
				msg = QtWidgets.QMessageBox(self)
				msg.setWindowTitle("Path doesn't exist")
				msg.setText("Entered config path doesn't exist.")
				msg.exec_()

	def expand_mpd_socket_path(self):
		path = self.inpConnHost.text()
		if "~" in path:
			print("[DBG] expanding path")
			h_path = os.path.expanduser("~")
			path = path.replace("~", h_path, 1)
			self.inpConnHost.setText(path)
	
	def test_mpd_socket_path(self):
		path = self.inpConnHost.text()
		if "~" in path or "home" in path or "var" in path:
			if not os.path.exists(path):
				msg = QtWidgets.QMessageBox(self)
				msg.setWindowTitle("Socket path doesn't exist")
				msg.setText("Socket at path %s does not exist. Make sure that:\n a. You have instance of MPD running,\n b. that path exists" % path)
				msg.exec_()
	
	def mpd_defaults(self):
		self.inpConnHost.setText("127.0.0.1")
		self.inpConnPort.setText("6600")
		self.inpConnAuth.setChecked(False)
		self.inpConnPwd.clear()
	
	def saveConfig(self):
		cfg = self.oldCfg
		
		# MPD stuffs first
		if "mpd" not in cfg:
			cfg['mpd'] = {}
		cfg['mpd']['host'] = self.inpConnHost.text()
		cfg['mpd']['port'] = self.inpConnPort.text()
		cfg['mpd']['auth'] = str(self.inpConnAuth.isChecked())
		cfg['mpd']['pswd'] = self.inpConnPwd.text()
		cfg['mpd']['config_path'] = self.inpMPDConfigDir.text()
		cfg['mpd']['playlist_path'] = self.inpMPDPlaylistDir.text()
		cfg['mpd']['music_path'] = self.inpMusicDir.text()
		
		# now, scrobbling
		if "last.fm" not in cfg:
			cfg['last.fm'] = {}
		cfg['last.fm']['enabled'] = str(self.chkLastFm.isChecked())
		cfg['last.fm']['username'] = self.inpLastFMusr.text()
		cfg['last.fm']['password'] = self.inpLastFMpwd.text()
		cfg['last.fm']['api_key'] = self.inpLastFMAPI.text()
		cfg['last.fm']['api_secret'] = self.inpLastFMShared.text()
		if "libre.fm" not in cfg:
			cfg['libre.fm'] = {}
		cfg['libre.fm']['enabled'] = str(self.chkLibreFm.isChecked())
		cfg['libre.fm']['username'] = self.inpLibreFMusr.text()
		cfg['libre.fm']['password'] = self.inpLibreFMpwd.text()
		if "local" not in cfg:
			cfg['local'] = {}
		cfg['local']['enabled'] = str(self.chkLocal.isChecked())
		cfg['local']['path'] = self.inpLogPath.text()
		cfg['local']['format'] = self.inpLogFormat.currentText()
		cfg['local']['perc_played'] = str(self.inpLogAfter.value())
		
		# general app settings
		if "general" not in cfg:
			cfg['general'] = {}
		cfg['general']['autoplay'] = str(self.chkPlayOnStart.isChecked())
		cfg['general']['autoplay_dynamic'] = str(self.chkPlayOnStartDynamic.isChecked())
		cfg['general']['exit_stop'] = str(self.chkStopOnExit.isChecked())
		cfg['general']['save_state'] = str(self.chkSaveOnExit.isChecked())
		cfg['general']['XFKeys'] = str(self.chkListenXFKeys.isChecked())
		cfg['general']['single_instance'] = str(self.chkOneInstanceOnly.isChecked())
		
		# notification settings
		if "notifications" not in cfg:
			cfg['notifications'] = {}
		cfg['notifications']['enabled'] = str(self.chkNotifications.isChecked())
		cfg['notifications']['now_playing'] = str(self.chkNotificationNowPlaying.isChecked())
		cfg['notifications']['play_state'] = str(self.chkNotificationStateChanged.isChecked())
		cfg['notifications']['db_update'] = str(self.chkNotificationDBUpdate.isChecked())
		cfg['notifications']['option_state'] = str(self.chkNotificationPlaybackChange.isChecked())
		cfg['notifications']['volume_change'] = str(self.chkNotificationVolume.isChecked())
		
		# dynamic playback
		if "player" not in cfg:
			cfg['player'] = {}
		cfg['player']['min'] = str(self.inpMinTracks.value())
		cfg['player']['max'] = str(self.inpMaxTracks.value())
		cfg['player']['ignore_artists'] = str(self.chkIgnoreArtists.isChecked())
		cfg['player']['ignored_artists'] = self.inpIgnoredArtists.text()
		cfg['player']['ignore_albums'] = str(self.chkIgnoreAlbums.isChecked())
		cfg['player']['ignored_albums'] = self.inpIgnoredAlbums.text()
		cfg['player']['ignore_genres'] = str(self.chkIgnoreGenres.isChecked())
		cfg['player']['ignored_genres'] = self.inpIgnoredGenres.text()
		cfg['player']['ignore_duration'] = str(self.chkIgnoreDuration.isChecked())
		cfg['player']['min_length'] = str(self.inpMinDuration.value())
		cfg['player']['max_length'] = str(self.inpMaxDuration.value())
				
		# and finally, tray settings
		if "tray_icon" not in cfg:
			cfg['tray_icon'] = {}
		cfg['tray_icon']['enabled'] = str(self.chkTrayIcon.isChecked())
		cfg['tray_icon']['volume_control'] = str(self.chkTrayVolume.isChecked())
		cfg['tray_icon']['close'] = str(self.chkTrayClose.isChecked())
		cfg['tray_icon']['minimize'] = str(self.chkTrayMinimize.isChecked())
		
		# now emit 'configChanged'
		self.settingsChanged.emit(cfg)
		
	def loadConfig(self):
		cfg = self.oldCfg
		
		# load only if CFG exists.
		if len(cfg) > 0:
			# populate only existing keys/sections
			# let's start with MPD settings
			if "mpd" in cfg:
				mpd_cfg = cfg["mpd"]
				self.inpConnHost.setText("%s" % mpd_cfg.get('host'))
				self.inpConnPort.setText("%s" % mpd_cfg.get('port'))
				self.inpConnAuth.setChecked(mpd_cfg.getboolean('auth', False))
				self.inpConnPwd.setText("%s" % mpd_cfg.get('pswd')) # todo: encrypt this shit
				self.inpMPDConfigDir.setText("%s" % mpd_cfg.get('config_path'))
				self.inpMPDPlaylistDir.setText("%s" % mpd_cfg.get('playlist_path'))
				self.inpMusicDir.setText("%s" % mpd_cfg.get('music_path'))
			
			# now, last.fm
			if "last.fm" in cfg:
				lastfm_cfg = cfg["last.fm"]
				self.chkLastFm.setChecked(lastfm_cfg.getboolean('enabled', False))
				self.inpLastFMusr.setText("%s" % lastfm_cfg.get('username'))
				self.inpLastFMpwd.setText("%s" % lastfm_cfg.get('password'))
				self.inpLastFMAPI.setText("%s" % lastfm_cfg.get('api_key', ""))
				self.inpLastFMShared.setText("%s" % lastfm_cfg.get('api_secret', ""))
				
			# now, libre.fm
			if "libre.fm" in cfg:
				librefm_cfg = cfg["libre.fm"]
				self.chkLibreFm.setChecked(librefm_cfg.getboolean('enabled', False))
				self.inpLibreFMusr.setText("%s" % librefm_cfg.get('username'))
				self.inpLibreFMpwd.setText("%s" % librefm_cfg.get('password'))	
			
			# now, local
			if "local" in cfg:
				local_cfg = cfg["local"]
				self.chkLocal.setChecked(local_cfg.getboolean('enabled', False))
				self.inpLogPath.setText("%s" % local_cfg.get('path'))
				self.inpLogFormat.setCurrentIndex(self.inpLogFormat.findText("%s" % local_cfg.get('format')))
				self.inpLogAfter.setValue(local_cfg.getint('perc_played'))
				
			# now, app settings:
			if "general" in cfg:
				app_cfg = cfg["general"]
				self.chkPlayOnStart.setChecked(app_cfg.getboolean('autoplay', False))
				self.chkPlayOnStartDynamic.setChecked(app_cfg.getboolean('autoplay_dynamic', 'True'))
				self.chkStopOnExit.setChecked(app_cfg.getboolean('exit_stop', False))
				self.chkSaveOnExit.setChecked(app_cfg.getboolean('save_state', False))
				self.chkListenXFKeys.setChecked(app_cfg.getboolean('XFKeys', True))
				self.chkOneInstanceOnly.setChecked(app_cfg.getboolean('single_instance', False))
			
			# now, notiication settins
			if "notifications" in cfg:
				notify_cfg = cfg["notifications"]
				self.chkNotifications.setChecked(notify_cfg.getboolean('enabled', True))
				self.chkNotificationNowPlaying.setChecked(notify_cfg.getboolean('now_playing', True))
				self.chkNotificationStateChanged.setChecked(notify_cfg.getboolean('play_state', True))
				self.chkNotificationDBUpdate.setChecked(notify_cfg.getboolean('db_update', True))
				self.chkNotificationPlaybackChange.setChecked(notify_cfg.getboolean('option_state', True))
				self.chkNotificationVolume.setChecked(notify_cfg.getboolean('volume_change', True))
			
			# dynamic playlist settings
			if "player" in cfg:
				player_cfg = cfg['player']
				self.inpMinTracks.setValue(player_cfg.getint('min', 10))
				self.inpMaxTracks.setValue(player_cfg.getint('max', 30))
				self.chkIgnoreArtists.setChecked(player_cfg.getboolean('ignore_artists', False))
				self.inpIgnoredArtists.setText(player_cfg.get('ignored_artists'))
				self.chkIgnoreAlbums.setChecked(player_cfg.getboolean('ignore_albums', False))
				self.inpIgnoredAlbums.setText(player_cfg.get('ignored_albums'))
				self.chkIgnoreGenres.setChecked(player_cfg.getboolean('ignore_genres', False))
				self.inpIgnoredGenres.setText(player_cfg.get('ignored_genres'))
				self.chkIgnoreDuration.setChecked(player_cfg.getboolean('ignore_duration', False))
				self.inpMinDuration.setValue(player_cfg.getint('min_length', 0))
				self.inpMaxDuration.setValue(player_cfg.getint('max_length', 0))
			
			# and finaly, tray icon!
			if "tray_icon" in cfg:
				tray_cfg = cfg["tray_icon"]
				self.chkTrayIcon.setChecked(tray_cfg.getboolean('enabled'))
				self.chkTrayVolume.setChecked(tray_cfg.getboolean('volume_control'))
				self.chkTrayClose.setChecked(tray_cfg.getboolean('close'))
				self.chkTrayMinimize.setChecked(tray_cfg.getboolean('minimize'))			
	
	def logAfterUpdate(self, value):
		self.txtLogAfterPerc.setText("%d %%" % value)
	
	def pageChanged(self):
		pageMPDState = self.btnMPDSets.isChecked()
		pageScrobblerState = self.btnScrobblerSets.isChecked()
		pagePlaybackState = self.btnPlaybackSets.isChecked()
		dynamicPlsState = self.btnDynamicPls.isChecked()
		notificationsState = self.btnNotifications.isChecked()
		
		if pageMPDState:
			self.stackedWidget.setCurrentIndex(0)
		elif pageScrobblerState:
			self.stackedWidget.setCurrentIndex(1)
		elif pagePlaybackState:
			self.stackedWidget.setCurrentIndex(2)
		elif dynamicPlsState:
			self.stackedWidget.setCurrentIndex(3)
		elif notificationsState:
			self.stackedWidget.setCurrentIndex(4)
		else:
			self.stackedWidget.setCurrentIndex(0)
