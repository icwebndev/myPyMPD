from PyQt5 import Qt, QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QVariant, Qt, QSortFilterProxyModel, QObject, pyqtSignal
from mpd import (MPDClient, CommandError)
from random import choice
from socket import error as SocketError
from sys import exit
from daemonize import Daemonize
from select import select
from datetime import datetime
import threading
import logging
import time
import os
import signal
import sys
import notify2
import configparser
from mpd_controler import MPDControler
from settings import myPyMPD_settings
from library import myPyMPD_library
from scrobbler import LocalScrobbler, LastFMScrobbler
import myPyMPD

class ListenerThread(QtCore.QThread):
	# listener -> ui
	playerUpdated = pyqtSignal(name="PlayerUpdated")
	playlistUpdated = pyqtSignal(name="PlaylistUpdated")
	databaseUpdated = pyqtSignal(name="DatabaseUpdated")
	databaseUpdateRunning = pyqtSignal(name="DatabaseUpdating")
	playbackOptionsUpdated = pyqtSignal(name="PlaybackUpdated")
	outputsChanged = pyqtSignal(name="OutputsChanged")
	MixerChanged = pyqtSignal(name="VolumeChanged")
	ControlerOpUpdate = pyqtSignal(int, int, bool, str)
	signal_loopActive = pyqtSignal(name="LoopActivated")
	# ui -> listener
	ToggleDynamic = pyqtSignal(bool, name="ToggleDynamic")
	RepopulatePlaylist = pyqtSignal(name="RepopulatePlaylist")
	ConfigChanged = pyqtSignal(configparser.ConfigParser, name="ConfigChanged")

	# TODO: for other signals, create callbacks...
	def __init__(self, name, sys_config, parent=None):
		super(ListenerThread, self).__init__(parent)
		self.name = name
		self.c = MPDControler(sys_config)
	def run(self):
		print("[init] Starting listener thread")
		# listener -> ui
		self.c.playerUpdate.connect(self.playerUpdateReceived)
		self.c.playlistUpdate.connect(self.playlistUpdateReceived)
		self.c.dbUpdateStarted.connect(self.dbUpdateReceived)
		self.c.dbUpdateFinished.connect(self.dbUpdated)
		self.c.playbackOptsChanged.connect(self.optionsChanged)
		self.c.outputsChanged.connect(self.outsChanged)
		self.c.volumeChanged.connect(self.volumeChanged)
		self.c.ControlerOperationNotification.connect(self.controlerUpdate)
		self.c.loopActivated.connect(self.loopActive)
		# ui -> listener
		self.ToggleDynamic.connect(self.toggleDyn)
		self.RepopulatePlaylist.connect(self.repopulatePl)
		self.ConfigChanged.connect(self.updateConfig)
		# start listening
		self.c.listener()
		print("[shutdown] stopping listener thread")
		
	def loopActive(self):
		self.signal_loopActive.emit()
	
	def controlerUpdate(self, curr, maxx, state, description):
		self.ControlerOpUpdate.emit(curr, maxx, state, description)
	
	def updateConfig(self, cfg):
		self.c.configUpdated.emit(cfg)
	
	def toggleDyn(self, state):
		self.c.dynamicModeChanged.emit(state)
	
	def repopulatePl(self):
		self.c.repopulatePlaylist.emit()
	
	def playerUpdateReceived(self):
		self.playerUpdated.emit()
	
	def playlistUpdateReceived(self):
		self.playlistUpdated.emit()
	
	def dbUpdateReceived(self):
		self.databaseUpdateRunning.emit()
	
	def dbUpdated(self):
		self.databaseUpdated.emit()
	
	def optionsChanged(self):
		self.playbackOptionsUpdated.emit()
	
	def outsChanged(self):
		self.outputsChanged.emit()
	
	def volumeChanged(self):
		self.MixerChanged.emit()
		
class CountdownThread(QtCore.QThread):
	signal = pyqtSignal(name="UpdateProgress")
	
	def __init__(self, name, active, parent=None):
		super(CountdownThread, self).__init__(parent)
		self.name = name
		self.active = active
		
	def run(self):
		print("[Tracker] Starting")
		while self.active:
			# send signal
			self.signal.emit()
			# sleep
			time.sleep(1)
			
		print("[Tracker] Exiting")
	
class myPyMPD(QtWidgets.QMainWindow, myPyMPD.Ui_MainWindow):
	app = {
		"name": "myPyMPD",
		"version": 1,
		"subversion": 0,
		"fix": 0,
		"date": 20170126
	}
	config_path = str("~/.config/%s/config" % app["name"])
	pid_path = str("~/.config/%s/pid" % app['name'])
	storage_path = str("~/.config/%s/" % app['name'])
	stop_after = None
	lastfm_scrobble = False
	play_queue = None
	
	def __init__(self):
		super(self.__class__, self).__init__()
		self.setupUi(self)
		
		# print welcome banner :D
		print("----------------------------------------------------")
		print("%s %d.%d.%d (%d) starting\n" % (self.app['name'], self.app['version'], self.app['subversion'], self.app['fix'], self.app['date']))

		
		# txtTrack is empty... fix that
		self.txtTrack.setText("%s %d.%d.%d (%d)" % (self.app['name'], self.app['version'], self.app['subversion'], self.app['fix'], self.app['date']))
		
		# initialise notifications
		notify2.init(self.app["name"])
		self.n = notify2.Notification("mpd", message="", icon="audio-x-generic")
		
		# hide titlebar from dockControls
		self.dockControls.setTitleBarWidget(QtWidgets.QWidget(self))
				
		# hide playlist loading progress bar
		self.progressBar.setVisible(False)
		
		# hide queue
		self.dockQueue.setVisible(False)
		
		# hide volume bar :D
		self.volumeBar.setVisible(False)
		
		# get configuration path
		user_home = os.path.expanduser('~')
		
		if os.path.exists(str("%s/.config" % user_home)):
			self.config_path = str("%s/.config/%s/config" % (user_home, self.app["name"]))
			self.pid_path = str("%s/.config/%s/pid" % (user_home, self.app["name"]))
			self.storage_path = str("%s/.config/%s/" % (user_home, self.app["name"]))
		else:
			self.config_path = str("%s/.%s/config" % (user_home, self.app["name"]))
			self.pid_path = str("%s/.%s/pid" % (user_home, self.app["name"]))
			self.storage_path = str("%s/.%s/" % (user_home, self.app["name"]))
	
		print("Configuration file: %s" % self.config_path)
		print("PID file: %s" % self.pid_path)
		print("Local storage path: %s\n" % self.storage_path)
		
		# create file if it doesn't exist
		if not os.path.exists(self.config_path):
			print("[Config] (check) Configuration file doesn't exist, may need to reconfigure everything!")
			self.settings()
		
		# initialize settings
		self.cfg_parser = configparser.ConfigParser()
		self.cfg_parser.read(self.config_path)			
		
		print("Configuration read success!")
		print("----------------------------------------------------")
		
		if "paths" not in self.cfg_parser:
			self.cfg_parser['paths'] = {}
		
		paths_updated = False
		if 'pid' not in self.cfg_parser['paths'] or self.cfg_parser['paths']['pid'] != self.pid_path:
			self.cfg_parser['paths']['pid'] = self.pid_path
			paths_updated = True
		if 'storage' not in self.cfg_parser['paths'] or self.cfg_parser['paths']['storage'] != self.storage_path:
			self.cfg_parser['paths']['storage'] = self.storage_path
			paths_updated = True
		
		if paths_updated:
			with open(self.config_path, 'w') as cfgfile:
				self.cfg_parser.write(cfgfile)
			cfgfile.close()
			self.cfg_parser.read(self.config_path)

		# check pid
		if "general" in self.cfg_parser:
			if self.cfg_parser['general'].getboolean("single_instance"):
				print("[Startup] Checking if there is another instance running")
				# check does instance exist
				pid = os.getpid()
				oldPid = self.get_pid()
				if int(oldPid) > -1:
					print("[Startup] (err) Only one instance allowed, and app is already active. If app crashed prevously, please remove pid file")
					msg = QtWidgets.QMessageBox()
					msg.setWindowTitle("Error!")
					msg.setText("Application is already active. Click OK to close all other instances and remove pid file")
					msg.addButton(QtWidgets.QMessageBox.Ok)
					msg.addButton(QtWidgets.QMessageBox.Cancel)
					e = msg.exec_()
					
					if e == QtWidgets.QMessageBox.Ok:
						os.remove(self.pid_path)
						try:
							os.kill(oldPid, signal.SIGTERM)
						except:
							print("[Startup] (dbg) Previous instance already dead.")
					else:
						self.exit()
				
				print("[Startup] writing pid file: %d" % pid)
				# write new pid
				with open(self.pid_path, 'w') as pidfile:
					pidfile.write(str(pid))
				pidfile.close()
		
		# call controller
		self.c = MPDControler(self.cfg_parser)
		self.c.ControlerOperationNotification.connect(self.operationProgress)
		
		# store status
		self.s = self.c.status()
		
		# call local scrobbler
		self.l_Scrobbler = None
		if self.cfg_parser['local'].getboolean('enabled'):
			self.l_Scrobbler = LocalScrobbler(self.cfg_parser['local'])
		
		self.last_fm = None
		if self.cfg_parser['last.fm'].getboolean('enabled'):
			self.last_fm = LastFMScrobbler(self.cfg_parser['last.fm'], self.cfg_parser['paths'])

		# 'Add to playlist' menu
		self.addTracks = QtWidgets.QMenu("Add to playlist")
		self.addTracks.setIcon(QtGui.QIcon.fromTheme("list-add"))
		self.addTracks.addAction(self.actionAddLibrary)
		self.addTracks.addAction(self.actionAdd_files)
		self.addTracks.addAction(self.actionAddRandom)
		self.addTracks.addSeparator()
		self.addTracks.addAction(self.actionLuckyArtist)
		self.addTracks.addAction(self.actionLuckyAlbum)
		self.btnAdd.setMenu(self.addTracks)
			
		# button & controls bindings
		# - volume bar
		self.volumeBar.valueChanged.connect(self.setVolume)
		# - shuffle, repeat, single, consume
		self.btnShuffle.toggled.connect(self.setPlaybackOptions)
		self.btnRepeat.toggled.connect(self.setPlaybackOptions)
		self.btnSingle.toggled.connect(self.setPlaybackOptions)
		self.btnConsume.toggled.connect(self.setPlaybackOptions)
		# -- for actions, too
		self.actionShuffle.toggled.connect(self.setPlaybackOptionsTray)
		self.actionRepeat.toggled.connect(self.setPlaybackOptionsTray)
		self.actionSingle.toggled.connect(self.setPlaybackOptionsTray)
		self.actionConsume.toggled.connect(self.setPlaybackOptionsTray)
		# - play/pause, next, prev, stop
		self.btnToggle.clicked.connect(self.toggle)
		self.btnStop.clicked.connect(self.stop)
		self.btnNext.clicked.connect(self.next_track)
		self.btnPrev.clicked.connect(self.prev_track)
		# -- for actions, too
		self.actionToggle.triggered.connect(self.toggle)
		self.actionStop.triggered.connect(self.stop)
		self.actionNext.triggered.connect(self.next_track)
		self.actionPrevious.triggered.connect(self.prev_track)
		# - dynamic mode
		self.btnDynamicMode.toggled.connect(self.dynamic)
		# - add to playlist (TODO - 5 actions!!!), remove, clear playlist, repopulate playlist
		# left to do: add files and add from library
		self.actionLuckyArtist.triggered.connect(self.feelingLuckyArtist)
		self.actionLuckyAlbum.triggered.connect(self.feelingLuckyAlbum)
		self.actionAddRandom.triggered.connect(self.addRandomTracks)
		self.actionAdd_files.triggered.connect(self.addFiles)
		self.actionAddLibrary.triggered.connect(self.showLibrary)
		self.btnOpenPls.clicked.connect(self.load_playlist)
		self.btnRemove.clicked.connect(self.deleteFromPlaylist)
		self.btnClear.clicked.connect(self.clearPlaylist)
		self.btnRepopulate.clicked.connect(self.repopulatePlaylist)
		# - scrobbling love
		self.btnLove.clicked.connect(self.last_fm_love)
		self.actionLoveTrack.triggered.connect(self.last_fm_love)
		# - Show music library
		self.btnShowLibrary.clicked.connect(self.showLibrary)
		self.actionLibrary.triggered.connect(self.showLibrary)
		# - show settings
		self.btnSettings.clicked.connect(self.settings)
		self.actionPreferences.triggered.connect(self.settings)
		# - link volume button to one more event - we have to save it's state
		self.btnVolume.toggled.connect(self.volumeToggled)
		# - call seek() on slider move by mouse
		self.progressSlider.sliderMoved.connect(self.seekTrack)
		# - bind table double click to  play
		self.tablePlaylist.doubleClicked.connect(self.next_track)
		# - bind playlist right click
		self.tablePlaylist.customContextMenuRequested.connect(self.playlistContextMenu)
		# - connect actions now
		self.actionMove_up.triggered.connect(self.moveUp)
		self.actionMove_to_top.triggered.connect(self.moveTop)
		self.actionMove_down.triggered.connect(self.moveDown)
		self.actionMove_to_bottom.triggered.connect(self.moveBottom)
		self.actionClear.triggered.connect(self.clearPlaylist)
		self.actionRepopulate.triggered.connect(self.repopulatePlaylist)
		self.actionStop_after_this_track.triggered.connect(self.stopAfter)
		self.actionQueue.triggered.connect(self.queueTrack)
		# - allow tracks to be moved around - only via buttons, sorry, no drag&drop
		self.btnMoveUp.clicked.connect(self.moveUp)
		self.btnMoveTop.clicked.connect(self.moveTop)
		self.btnMoveDown.clicked.connect(self.moveDown)
		self.btnMoveBottom.clicked.connect(self.moveBottom)
		# - enable last.fm scrobbling toggle - in case you don't want something to scrobble.
		# --> if local scrobbling is enabled, track will be saved, so I got you covered.
		self.btnToggleLastFM.toggled.connect(self.toggleLastFmScrobble)
		self.actionToggleLastFM.toggled.connect(self.toggleLastFmScrobble)
		# - hide and exit actions for tray icon
		self.actionToggleVisibility.triggered.connect(self.toggleVisibility)
		self.actionExit.triggered.connect(self.exit)
		# - Queue list buttons
		self.btnClearQueue.clicked.connect(self.clearQueue)
		self.btnRemoveQueue.clicked.connect(self.removeFromQueue)
		# - seek forward and backward buttons
		self.btnRewindBack.pressed.connect(self.seek_minus10)
		self.btnRewindForward.pressed.connect(self.seek_plus10)
		
		# set table filter... 
		self.npFilter = QtCore.QSortFilterProxyModel()
		self.npFilter.setDynamicSortFilter(True)
		self.tablePlaylist.setModel(self.npFilter)
		
		# set Queue filter
		self.queueFilter = QtCore.QSortFilterProxyModel()
		self.queueFilter.setDynamicSortFilter(True)
		self.listQueue.setModel(self.queueFilter)
		
		# Timer thread...
		self.TrackTimer = None
		
		# MPD thread
		self.t = ListenerThread("ListenerThread", self.cfg_parser)
		
		# setup logging
		self.logger = logging.getLogger(__name__)
		self.logger.setLevel(logging.DEBUG)
		self.logger.propagate = True
		fh = logging.FileHandler("%s/myPyMPD_log" % self.storage_path, "a")
		fh.setLevel(logging.DEBUG)
		self.logger.addHandler(fh)
		keep_fds = [fh.stream.fileno()]
		
		# show playlist
		self.setPlaylist()
		
		# set queue
		if not os.path.exists("%s/current_queue" % self.storage_path):
			self.updateQueueList(None)
		
		# bind filter to proxy.
		self.inpSearch.textChanged.connect(self.filterPlaylist)
		
		# status update!
		self.setStatus()
		
		# set icon for toggleLastFM action...
		fmIcon = QtGui.QIcon()
		fmIcon.addPixmap(QtGui.QPixmap(":/icons/icons/lastfm.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
		self.actionToggleLastFM.setIcon(fmIcon)
		
		# disable last.fm actions if last.fm isn't enabled
		if not self.cfg_parser['last.fm'].getboolean('enabled'):
			self.actionLoveTrack.setEnabled(False)
			self.btnLove.setEnabled(False)
			self.btnBanTrack.setEnabled(False)
			self.actionToggleLastFM.setEnabled(False)
			self.btnToggleLastFM.setEnabled(False)
		
		# add tray, if wanted
		if self.cfg_parser['tray_icon'].getboolean('enabled'):
			self.tray = SystemTrayIcon(QtGui.QIcon.fromTheme("audio-x-generic"))
			self.tray.menu.addSection("%s" % (self.app["name"]))
			self.tray.menu.addAction(self.actionPlaybackStatus)
			self.tray.menu.addAction(self.actionToggleLastFM)
			self.tray.menu.addAction(self.actionLoveTrack)
			self.tray.menu.addSection("Controls")
			self.tray.menu.addAction(self.actionToggle)
			self.tray.menu.addAction(self.actionStop)
			self.tray.menu.addAction(self.actionNext)
			self.tray.menu.addAction(self.actionPrevious)
			self.tray.menu.addSection("Playlist")
			self.tray.menu.addMenu(self.addTracks)
			self.tray.menu.addAction(self.actionRepopulate)
			self.tray.menu.addAction(self.actionClear)
			self.tray.menu.addSection("Playback")
			self.tray.menu.addAction(self.actionShuffle)
			self.tray.menu.addAction(self.actionRepeat)
			self.tray.menu.addAction(self.actionConsume)
			self.tray.menu.addAction(self.actionSingle)
			self.tray.menu.addMenu(self.menuOutputs)
			self.tray.menu.addSeparator()
			self.tray.menu.addAction(self.actionToggleVisibility)
			self.tray.menu.addAction(self.actionExit)
			
			# must set this before show...
			if self.isVisible():
				self.actionToggleVisibility.setIcon(QtGui.QIcon.fromTheme('view-fullscreen'))
				self.actionToggleVisibility.setText("Restore window")
			else:
				self.actionToggleVisibility.setIcon(QtGui.QIcon.fromTheme('view-restore'))
				self.actionToggleVisibility.setText("Hide window")
				
			self.tray.show()
		
		# connect to system tray signal
		self.tray.activated.connect(self.systemTraySlot)
		# Set menu for outputs
		self.outputList = QtWidgets.QMenu("Outputs")
		self.outputList.setIcon(QtGui.QIcon.fromTheme("audio-card"))
		self.addOutputs()
		
		# bind to controler signals and start it
		self.t.playerUpdated.connect(self.setStatus)
		self.t.playlistUpdated.connect(self.setPlaylist)
		self.t.playbackOptionsUpdated.connect(self.updateOptions)
		self.t.MixerChanged.connect(self.updateVolume)
		self.t.ControlerOpUpdate.connect(self.operationProgress)
		# connect the loopActive signal to afterLoad, in case we have DynamicMode enabled or so.
		self.t.signal_loopActive.connect(self.afterLoad)
		# start listening to MPD changes...
		self.t.start()
	
	def seek_plus10(self):
		c = self.c
		
		if not c.connected:
			c.connect()
		
		c.client.seekcur("+10")
	
	def seek_minus10(self):
		c = self.c
		
		if not c.connected:
			c.connect()
		
		c.client.seekcur("-10")
	
	def toggleLastFmScrobble(self, state):
		if self.lastfm_scrobble != state:
			if state:
				print("[last.fm] scrobbling (re)enabled")
			else:
				print("[last.fm] scrobbling paused")
		
		
		if self.lastfm_scrobble != state:
			self.lastfm_scrobble = state
		
		if self.btnToggleLastFM.isChecked() != state:
			self.btnToggleLastFM.setChecked(state)
		if self.actionToggleLastFM.isChecked() != state:
			self.actionToggleLastFM.setChecked(state)
		
		# save state
		if "ui_states" not in self.cfg_parser:
			self.cfg_parser["ui_states"] = {}
			oldState = False
		else:
			oldState = self.cfg_parser["ui_states"].getboolean("last.fm", True)
		
		if oldState != state:	
			self.cfg_parser["ui_states"]["last.fm"] = str(state)
			with open(self.config_path, 'w+') as configfile:
				self.cfg_parser.write(configfile)
			configfile.close()
	
	def last_fm_love(self):
		if not self.c.connected:
			self.c.connect()
		
		np = self.c.now_playing()
		
		self.last_fm.love_track(np['artist'], np['track'])
		
		self.notify("Last.fm", "Loved:\n%s by %s" % (np['track'], np['artist']), "emblem-favorite")
	
	oldVolume = 0
	def systemTraySlot(self, signal):
		if signal == QtWidgets.QSystemTrayIcon.Trigger:
			self.toggleVisibility()
		
		elif self.cfg_parser['tray_icon'].getboolean('volume_control'):
			if signal == QtWidgets.QSystemTrayIcon.DoubleClick:
				self.toggle()
			elif signal == QtWidgets.QSystemTrayIcon.MiddleClick:
				if self.oldVolume == 0:
					self.oldVolume = self.volumeBar.value()
					newVolume = 0
				else:
					newVolume = self.oldVolume
					self.oldVolume = 0
				
				if not self.c.connected:
					self.c.connect()
	
				self.c.client.setvol(newVolume)
	
	def exit(self):
		# Cleanup...
		self.closeEventCleanUp()
		print("[Shutdown] Quiting!")
		QtWidgets.QApplication.quit()
		sys.exit()
	
	def toggleVisibility(self):
		if self.isVisible():
			print("[Runtime] Hiding main window!")
			self.hide()
			self.actionToggleVisibility.setIcon(QtGui.QIcon.fromTheme('view-fullscreen'))
			self.actionToggleVisibility.setText("Restore window")
		else:
			print("[Runtime] Showing main window!")
			self.show()
			self.raise_()
			self.activateWindow()
			self.actionToggleVisibility.setIcon(QtGui.QIcon.fromTheme('view-restore'))
			self.actionToggleVisibility.setText("Hide window")
	
	def feelingLuckyArtist(self):
		c = self.c
		
		c.feelingLucky_artist()
	
	def feelingLuckyAlbum(self):
		c = self.c
		
		c.feelingLucky_album()
	
	def addRandomTracks(self):
		ok = False
		inp = QtWidgets.QInputDialog.getInt(self, "Add random tracks", "How many tracks should I add?", self.cfg_parser['player'].getint('min'), 0, 1000, 1)
		
		if inp != None and inp[0] > 0 and bool(inp[1]) == True:
			self.c.add_random(inp[0])
	
	def addFiles(self):
		c = self.c
		
		d = c.client.decoders()
		supported_formats = []
		for z in d:
			if 'suffix' in z:
				if type(z['suffix']) == str:
					supported_formats.append(z['suffix'])
				else:
					for s in z['suffix']:
						supported_formats.append(s)
			#for s in z['suffix']:
			#	supported_formats.append(s)
		
		str_formats = " *.".join(supported_formats)
		
		dialog = QtWidgets.QFileDialog(self, "Select audio files", self.cfg_parser['mpd'].get('music_path'))
		dialog.setFileMode(QtWidgets.QFileDialog.ExistingFiles)
		dialog.setNameFilter("Supported audio formats (%s)" % str_formats)
		
		files = []
		
		if dialog.exec_():
			files = dialog.selectedFiles()
			self.c.addFiles(files)

	def closeEventCleanUp(self):
		main_instance = True
		if self.get_pid() > 0:
			if self.get_pid() == os.getpid():
				print("[Shutdown] removing pid file")
				os.remove(self.pid_path)
			else:
				print("[Startup] Closing second %s instance" % self.app['name'])
				main_instance = False
		
		if self.cfg_parser['general'].getboolean('exit_stop'):
			if main_instance:
				print("[Shutdown] Stopping playback")
				self.c.client.stop()
		
	def closeEvent(self, event):
		if not self.cfg_parser['tray_icon'].getboolean('close'):
			self.closeEventCleanup()	
		else:
			print("[Runtime] Hiding to tray...")
			self.hide()
			event.ignore()
	
	def changeEvent(self, event):
		if event.type() == QtCore.QEvent.WindowStateChange:
			if self.windowState() and QtCore.Qt.WindowMinimized:
				if self.cfg_parser['tray_icon'].getboolean('minimize'):
					#QtCore.QTimer.singleShot(0, self, self.toggleVisibility)
					self.toggleVisibility()
	
	def get_pid(self):
		pid = -1
		if os.path.exists(self.pid_path):
		
			f = open(self.pid_path, 'r')
			spid = f.readline()
			f.close()
			
			pid = int(spid)
			
		return pid
			

	def afterLoad(self):
		c = self.c
		
		# restore states for 'Dynamic' and 'Volume' buttons. Doing that here to simply trigger their actions, if necessary.
		if "ui_states" in self.cfg_parser:
			if self.cfg_parser["ui_states"].getboolean("dynamic", False):
				self.btnDynamicMode.setChecked(True)
				self.actionRepopulate.setEnabled(True)
			if self.cfg_parser["ui_states"].getboolean("volume", False):
				self.btnVolume.setChecked(True)
			if self.cfg_parser["ui_states"].getboolean("last.fm", True):
				self.btnToggleLastFM.setChecked(True)
				self.lastfm_scrobble = True
		
		if self.cfg_parser['general'].getboolean('save_state'):
			self.reloadQueue()
		
		if "general" in self.cfg_parser:
			gen = self.cfg_parser['general']
			if gen.getboolean('autoplay'):
				print("[Runtime] (init) Autoplay enabled...")
				if gen.getboolean('autoplay_dynamic') and not self.btnDynamicMode.isChecked():
					print("[Runtime] (init) Dynamic autoplay is enabled, starting...")
					self.btnDynamicMode.setChecked(True)
				else:
					if self.npFilter.rowCount() == 0:
						if gen.getboolean('autoplay_dynamic'):
							self.btnDynamicMode.setChecked(True)
						else:
							print("[Runtime] (init) Playlist is empty, not starting playback.")
					else:
						if c.status()["state"] != "play":
							print("[Runtime] (init) Starting playback!")
							c.client.play(0)
			
	def stopAfter(self):
		selection = self.tablePlaylist.selectionModel()
		
		if selection.hasSelection():
			modelIndex = selection.selectedRows()[0]
			
		row = modelIndex.row()
		
		trackID = self.npFilter.index(row, 1).data()
		
		if self.stop_after == None or self.stop_after != trackID:
			print("[Playback] (info) stopping after %s" % trackID)
			self.stop_after = trackID

		else:
			print("[Playback] (info) toggled 'stop after'. Not stopping after %s" % trackID)
			self.stop_after = None
		
		self.setStatus()
		
	def moveTop(self):
		selection = self.tablePlaylist.selectionModel()
		
		if selection.hasSelection():
			c = self.c
			
			selectedRows = selection.selectedRows()
			total = len(selectedRows)
			top = 0
			
			for item in selectedRows:
				row = item.row()
				trackID = self.npFilter.index(row, 1).data()
				
				# move
				c.client.moveid(trackID, top)
				
				if total > 0:
					top+= 1
			
	def moveUp(self):
		selection = self.tablePlaylist.selectionModel()
		
		if selection.hasSelection():
			c = self.c
			
			selectedRows = selection.selectedRows()
			total = len(selectedRows)
			
			for item in selectedRows:
				row = item.row()
				trackID = self.npFilter.index(row, 1).data()
				
				top = row-1
				# move
				c.client.moveid(trackID, top)
	
	def moveBottom(self):
		selection = self.tablePlaylist.selectionModel()
		
		if selection.hasSelection():
			c = self.c
			
			selectedRows = selection.selectedRows()
			total = len(selectedRows)
			bottom = self.npFilter.rowCount()
			
			for item in reversed(selectedRows):
				row = item.row()
				trackID = self.npFilter.index(row, 1).data()
				
				# move
				c.client.moveid(trackID, bottom-1)
				
				if total > 0:
					bottom-= 1
	
	def moveDown(self):
		selection = self.tablePlaylist.selectionModel()
		
		if selection.hasSelection():
			c = self.c
			
			selectedRows = selection.selectedRows()
			total = len(selectedRows)
			rows = self.npFilter.rowCount()
			
			for item in reversed(selectedRows):
				row = item.row()
				if row+1 < rows:
					bottom = row+1
					
					trackID = self.npFilter.index(row, 1).data()
					
					# move
					c.client.moveid(trackID, bottom)
					
					if total > 0:
						bottom-= 1
	
	def get_max_priority(self):
		c = self.c
		
		pl = c.current_playlist()
		
		prios = []
		
		for x in pl[1:len(pl)-1]:
			if int(x[2]) not in prios:
				if int(x[2]) > 0: # add it only if it's bigger than 0.
					prios.append(int(x[2]))
		
		if len(prios) > 0:
			maximum = min(prios)-1
		else:
			maximum = 255
		
		return maximum
		
	
	model_data = []
	def queueTrack(self):
		c = self.c
		
		selection = self.tablePlaylist.selectionModel()
		
		if selection.hasSelection():
		
			modelIndexList = selection.selectedRows()
			priority = self.get_max_priority()
			status = c.status()
			curr_pos = int(status['song'])+1
						
			for modelIndex in modelIndexList:
				row = modelIndex.row()			
				trackID = self.npFilter.index(row, 1).data()
				artist = self.npFilter.index(row, 3).data()
				title = self.npFilter.index(row, 4).data()
				album = self.npFilter.index(row, 5).data()
				dur = self.npFilter.index(row, 6).data()
				
				if self.btnShuffle.isChecked():
					print("[DBG] adding %s with priority %d" % (trackID, priority))
					c.client.prioid(priority, trackID)
					priority-=1
				else:
					# no priority mode when shuffle is off - we're simply moving it to be right below currently playing track.
					#c.client.moveid(trackID, curr_pos)
					#curr_pos+=1
					"""
					this has to be redone, simply because if user adds tracks one by one, it will just add new tracks on top... and we don't want that.
					
					probably will have to implement something in setStatus() and hunt for playback end
					"""
					if self.play_queue == None:
						self.play_queue = []
					
					print("[Playlist] (queue) adding %s by %s (album: %s, id: %s) to queue" % (title, artist, album, trackID))
					self.play_queue.append({'id': trackID,'artist': artist, 'title': title, 'album': album, 'duration': dur})
				
				self.model_data.append({'id': trackID,'artist': artist, 'title': title, 'album': album, 'duration': dur})
			
			self.updateQueueList(self.model_data)
	
	def clearQueue(self):
		if self.btnShuffle.isChecked():
			c = self.c
			
			if not c.connected:
				c.connect()
			
			print("[Queue] Clear queue called - shuffle mode: cleaning all prios set.")
			for t in self.model_data:
				if self.isTrackInPlaylist(t['id']):
					c.client.prioid(0, t['id'])
				
		print("[Queue] (clear) cleaning all variables and data models...")
		self.model_data = []
		self.play_queue = None
		self.queueModel.clear()
		
		self.updateQueueList(self.model_data)
		
		if os.path.exists("%s/current_queue" % self.storage_path):
			print("[Queue] Removing stored queue...")
			os.remove("%s/current_queue" % self.storage_path)
		
		print("[Queue] queue cleared.")
		
	def removeFromQueue(self):
		selection = self.listQueue.selectionModel()
		
		if selection.hasSelection():
			ModelIndexList = selection.selectedRows()
			
			for modelIndex in reversed(ModelIndexList):
				r = modelIndex.row()
				
				# remove data from queue
				if self.play_queue != None:
					self.play_queue.pop(r)
					
				self.model_data.pop(r)
				
				# and now real fuckery
				if self.btnShuffle.isChecked():
					trackID = self.queueFilter.index(r, 0).data()
					
					c = self.c
					
					if not c.connected:
						c.connect()
					
					c.client.prioid(0, trackID)
			
			# update lis tnow
			self.updateQueueList(self.model_data)
	
	queueModel = None
	
	def setQueueList(self, data):
		cols = 5
		
		if data == None and self.queueModel == None:
			self.queueModel = QtGui.QStandardItemModel(0, cols)
		else:
			self.queueModel = QtGui.QStandardItemModel(len(data), cols)
		
		header_data = ["ID", "Artist", "Title", "Album", "Duration"]
		i = 0
		for h in header_data:
			self.queueModel.setHeaderData(i, QtCore.Qt.Horizontal, h)
			i+=1
		
		if data != None and len(data) > 0:
			j = 0
			for r in data:
				self.queueModel.setItem(j, 0, QtGui.QStandardItem(str(r["id"])))
				self.queueModel.setItem(j, 1, QtGui.QStandardItem(r["artist"]))
				self.queueModel.setItem(j, 2, QtGui.QStandardItem(r["title"]))
				self.queueModel.setItem(j, 3, QtGui.QStandardItem(r["album"]))
				self.queueModel.setItem(j, 4, QtGui.QStandardItem(r["duration"]))
				j+=1
		
		self.queueFilter.setSourceModel(self.queueModel)
		self.listQueue.hideColumn(0)
		self.listQueue.resizeColumnsToContents()
	
	def updateQueueList(self, data):
		cols = 5
		
		if data == None and self.queueModel == None:
			self.queueModel = QtGui.QStandardItemModel(0, cols)
		else:
			self.queueModel = QtGui.QStandardItemModel(len(data), cols)
		
		header_data = ["ID", "Artist", "Title", "Album", "Duration"]
		i = 0
		for h in header_data:
			self.queueModel.setHeaderData(i, QtCore.Qt.Horizontal, h)
			i+=1
		
		if data != None and len(data) > 0:
			j = 0
			for r in data:
				self.queueModel.setItem(j, 0, QtGui.QStandardItem(str(r["id"])))
				self.queueModel.setItem(j, 1, QtGui.QStandardItem(r["artist"]))
				self.queueModel.setItem(j, 2, QtGui.QStandardItem(r["title"]))
				self.queueModel.setItem(j, 3, QtGui.QStandardItem(r["album"]))
				self.queueModel.setItem(j, 4, QtGui.QStandardItem(r["duration"]))
				j+=1
		
		self.queueFilter.setSourceModel(self.queueModel)
		self.listQueue.hideColumn(0)
		self.listQueue.resizeColumnsToContents()
		
		# update nowplaying text
		self.set_status_text()
		
		# write queue to file, in case user wants us to save/restore states
		if self.cfg_parser['general'].getboolean('save_state'):
			if data != None:
				if len(data) > 0:
					print("[Queue] Saving current queue")
					# save current state - maximum priority, playback mode etc etc
					# in case user changes any of those in other client, we'll simply invalidate base and delete it
					# ofc. all of that will happen if queue was stored with random mode
					# also, due to this fuckery, I now have to check if queued track is actually in the playlist.
					# but that's a job for setStatus(), which is controlling playback.
					import pickle
					s_data = data.copy()
					s_data.insert(0, {"shuffle": str(self.btnShuffle.isChecked()), "max_prio": self.get_max_priority()})
					
					with open("%s/current_queue" % self.storage_path, 'wb') as queue:
						pickle.dump(s_data, queue, pickle.HIGHEST_PROTOCOL)
						queue.close()
				else:
					print("[Queue] Queue empty")
					if os.path.exists("%s/current_queue" % self.storage_path):
						os.remove("%s/current_queue" % self.storage_path)
			else:
				if os.path.exists("%s/current_queue" % self.storage_path):
					print("[Queue] queue finished/cleared, removing state file")
					os.remove("%s/current_queue" % self.storage_path)
	
	def reloadQueue(self):
		if os.path.exists("%s/current_queue" % self.storage_path):
			print("[Queue] Loading stored queue")
			import pickle
			
			q_data = None
			
			try:
				with open("%s/current_queue" % self.storage_path, 'rb') as queue:
					q_data = pickle.load(queue)
					queue.close()
				
				state = q_data[0]
				tracks = q_data[1:len(q_data)]
			
				all_ok = True
			except:
				print("[Queue] (err) trouble reading stored queue, invalidating!")
				all_ok = False
			
			# update nowplaying text
			self.set_status_text()
		
			# check states
			if all_ok and state['shuffle'] != str(self.btnShuffle.isChecked()):
				all_ok = False
			
			if all_ok and int(state['max_prio']) != int(self.get_max_priority()):
				all_ok = False
			
			if all_ok:
				# Check if tracks are in queue, and if shuffle isn't enabled, remove ones which aren't on the playlist anymore
				i = 0
				trcks = tracks.copy()
				for t in trcks:
					if self.isTrackInPlaylist(t['id']):
						print("[Queue] (load) found %s - %s (ID: %s)" % (t['artist'], t['title'], t['id']))
					else:
						print("[Queue] (load) %s - %s (ID: %s) removed from playlist" % (t['artist'], t['title'], t['id']))
						trcks.pop(i)
					
					i+=1
				
				# set queue
				if not self.btnShuffle.isChecked():
					self.play_queue = tracks # for player to know what to do
				
				self.model_data = tracks # for playlist.
				
				# show table
				self.setQueueList(trcks)
			else:
				print("[Queue] (load) play queue invalidated - play priority or play mode changed externally")
				os.remove("%s/current_queue" % self.storage_path)
		else:
			print("[Queue] (load) No stored queue found, skipping.")
				
	def isTrackInPlaylist(self, TrackID):
		c = self.c
		
		if not c.connected:
			c.connect()
		
		playlist = c.current_playlist()
		
		found = False
		
		for t in playlist[1:len(playlist)-1]:
			if int(t[1]) == int(TrackID): # to make sure we're comparing same types, python is sometimes fucky
				found = True
		
		return found
	
	def playlistContextMenu(self, position):
		selekcija = self.tablePlaylist.selectionModel()
		
		selected_items = selekcija.hasSelection()
		selected_counts = len(selekcija.selectedRows())
		random_playback = self.btnShuffle.isChecked()
		dynamic_playback = self.btnDynamicMode.isChecked()
		
		playlistContext = QtWidgets.QMenu(self)
		if selected_items:
			playlistContext.addAction(self.actionQueue)
			
			if selected_counts == 1:
				playlistContext.addAction(self.actionStop_after_this_track)
			
			playlistContext.addAction(self.actionRemove)
			
			playlistContext.addSeparator()
			
			if not random_playback:
				playlistMoveMenu = QtWidgets.QMenu("Move...", self)
				playlistMoveMenu.addAction(self.actionMove_to_top)
				playlistMoveMenu.addAction(self.actionMove_up)
				playlistMoveMenu.addAction(self.actionMove_down)
				playlistMoveMenu.addAction(self.actionMove_to_bottom)
				
				playlistContext.addMenu(playlistMoveMenu)
				playlistContext.addSeparator()
		
		playlistAddMenu = QtWidgets.QMenu("Add..", self)
		playlistAddMenu.addAction(self.actionAddRandom)
		playlistAddMenu.addAction(self.actionAddLibrary)
		playlistAddMenu.addAction(self.actionAdd_files)
		playlistAddMenu.addSeparator()
		playlistAddMenu.addAction(self.actionLuckyArtist)
		playlistAddMenu.addAction(self.actionLuckyAlbum)
		playlistContext.addMenu(playlistAddMenu)
		
		playlistContext.addAction(self.actionClear)
		if dynamic_playback:
			playlistContext.addAction(self.actionRepopulate)
	
		playlistContext.popup(self.tablePlaylist.viewport().mapToGlobal(position))
		
	
	def operationProgress(self, current, maximum, state, description):
		if not self.progressBar.isVisible():
			self.progressBar.setVisible(True)
			print("[Background] starting %s" % description)
			
		curmax = self.progressBar.maximum()
		
		if curmax != maximum:
			self.progressBar.setMaximum(maximum)
			
		self.progressBar.setValue(current)
		self.progressBar.setFormat(description)
		
		if current >= maximum:
			print("[Background] finished %s" % description)
			self.progressBar.setVisible(False)
	
	oldLastFm = None
	oldTray = None
	oldMPD = None
	def settings(self):
		self.oldLastFm = dict(self.cfg_parser['last.fm'])
		self.oldTray = dict(self.cfg_parser['tray_icon'])
		self.oldMPD = dict(self.cfg_parser['mpd'])
		
		c_dialog = myPyMPD_settings(self.cfg_parser)
		c_dialog.settingsChanged.connect(self.updateConfig)
		c_dialog.exec_()
	
	def updateConfig(self, cfg):
		print("[Config] configuration changed, saving and reloading!")
		
		# check if restart is necessary
		restrt = False
		
		for k in self.oldMPD.keys():
			if self.oldMPD[k] != cfg['mpd'][k]:
				restrt = True
		
		for k in self.oldLastFm.keys():
			if self.oldLastFm[k] != cfg['last.fm'][k]:
				restrt = True
		
		for k in self.oldTray.keys():
			if self.oldTray[k] != cfg['tray_icon'][k]:
				restrt = True		
		
		if len(cfg) > 0:
			with open(self.config_path, 'w+') as configfile:
				cfg.write(configfile)
			self.cfg_parser = cfg
			# Emit this one to ListenerThread, so it can reemit it to MPDControler.listener()
			self.t.ConfigChanged.emit(cfg)
		
		if restrt:
			msg = QtWidgets.QMessageBox(self)
			msg.setWindowTitle("Settings changed")
			msg.setText("To apply mpd, last.fm and tray icon settings, you  must restart app first.")
			msg.exec_()

	def notify(self, titl, msg, icon="audio-x-generic"):
		if self.cfg_parser['notifications'].getboolean('enabled'):
			try:
				if self.n == None:
					notify2.init(self.app["name"])
					self.n = notify2.Notification("mpd", message=msg, icon=icon)
				else:
					self.n.update(titl, message=msg, icon=icon)
				
				self.n.show()
			except:
				print("[Notifications] (err) Notification error: "+str(sys.exc_info()[1]))
				self.n = None
		else:
			print("[Notifications] (err) Notifications are disabled. Blocked notification %s: %s. Attend to it!" % (titl, msg))
	
	def showLibrary(self):
		self.library = myPyMPD_library(self.cfg_parser)
		self.library.signal_addToPlaylist.connect(self.addtoPls)
		self.library.signal_replacePlaylist.connect(self.replacePls)
		self.library.show()
	
	def addtoPls(self, tracks):
		c = self.c
		
		if not c.connected:
			c.connect()
		
		for t in tracks:
			c.client.add(t)
	
	def replacePls(self, tracks):
		c = self.c
		
		if not c.connected:
			c.connect()
		
		c.client.clear()
		
		for t in tracks:
			c.client.add(t)
		
		if self.btnDynamicMode.isChecked():
			self.btnDynamicMode.setChecked(False)
			
		c.client.play(0)
	
	def addOutputs(self):
		c = self.c
		
		if not self.outputList.isEmpty():
			self.menuOutputs.clear()
		
		if not c.connected:
			c.connect()
	
		outputs = c.client.outputs()
		for out in outputs:
			print("[Outputs] adding output %s with state %s" %(out['outputname'], out['outputenabled']))
			outItem = QtWidgets.QAction(out['outputname'], self)
			itemState = bool(int(out['outputenabled']))
			outItem.setData(out['outputid'])
			outItem.setCheckable(True)
			outItem.setChecked(itemState)
			outItem.triggered.connect( lambda checked, outid=out['outputid']: self.outputToggled(checked, outid) )
			
			self.menuOutputs.addAction(outItem)
		
		self.btnOutputs.setMenu(self.menuOutputs)
		
		# add tray menu, if tray is enabled
			
	
	def outputToggled(self, state, output):
		c = self.c
		
		if not c.connected:
			c.connect()
		
		print("[Outputs] outputID %s changed state to %s" % (output, str(state)))
		c.client.toggleoutput(output)
		
	def updateOptions(self):
		# store old options
		oldSets = self.s
		# "local" client
		c = self.c
		
		# configure new options
		self.s = c.status()
		s = self.s
		
		changes = ""
		
		if int(s["repeat"]) != int(oldSets["repeat"]):
			repeat = "On" if int(s["repeat"]) == 1 else  "Off"
			print("[Playback] (mode) Repeat: %s " % repeat)
			changes += str("Repeat: %s\n" % repeat)
			self.btnRepeat.setChecked(bool(int(s["repeat"])))
			
			if self.cfg_parser['tray_icon'].getboolean('enabled'):
				self.actionRepeat.setChecked(bool(int(s["repeat"])))
		
		if int(s["random"]) != int(oldSets["random"]):
			random = "On" if int(s["random"]) == 1 else  "Off"
			print("[Playback] (mode) Random/Shuffle: %s " % random)
			changes += str("Shuffle: %s\n" % random)
			self.btnShuffle.setChecked(bool(int(s["random"])))
			
			if self.cfg_parser['tray_icon'].getboolean('enabled'):
				self.actionShuffle.setChecked(bool(int(s["random"])))
			
		if int(s["consume"]) != int(oldSets["consume"]):			
			consume = "On" if int(s["consume"]) == 1 else  "Off"
			print("[Playback] (mode) Consume: %s " % consume)
			changes += str("Consume: %s\n" % consume)
			self.btnConsume.setChecked(bool(int(s["consume"])))
			
			if self.cfg_parser['tray_icon'].getboolean('enabled'):
				self.actionConsume.setChecked(bool(int(s["consume"])))
		
		if int(s["single"]) != int(oldSets["single"]):			
			single = "On" if int(s["single"]) == 1 else  "Off"
			print("[Playback] (mode) Single: %s " % single)
			changes += str("Single: %s\n" % single)
			self.btnSingle.setChecked(bool(int(s["single"])))
			
			if self.cfg_parser['tray_icon'].getboolean('enabled'):
				self.actionSingle.setChecked(bool(int(s["single"])))
		
		if self.cfg_parser['notifications'].getboolean('option_state'):
			if len(changes) > 0:
				self.notify("mpd: playback options", changes)
	
	def next_track(self, ModelIndex=None):
		c = self.c
		
		if not c.connected:
			c.connect()
			
		if self.npFilter.rowCount() > 1:
			# check if doubleclicked row
			if ModelIndex != None and type(ModelIndex) == QtCore.QModelIndex:
				row = ModelIndex.row()
				songID = self.npFilter.index(row, 1).data()
				
				c.client.playid(songID)
			else:
				s = c.status()
				if s["state"] == "play":
					c.client.next()
				else:
					if int(s["random"]) == 1:
						from random import randrange
						pos = randrange(0, self.npFilter.rowCount())
					else:
						pos = 0
					
					c.client.play(0)
						
	
	def prev_track(self):
		c = self.c
		s = c.status()
		
		if not c.connected:
			c.connect()
			
		if not bool(int(s["consume"])):
			if self.npFilter.rowCount() > 1 and int(s['song']) > 0:
				c.client.previous()
		else:
			c.client.seekcur(0)
	
	def toggle(self):
		c = self.c
		s = self.s
		
		if not c.connected:
			c.connect()
			
		if s["state"] == "pause":
			c.client.pause(0)
		else:
			if s["state"] == "play":
				c.client.pause(1)
			else:
				if self.npFilter.rowCount() > 0:
					c.client.play(0)
	
	def load_playlist(self):
		c = self.c
		
		if not c.connected:
			c.connect()
			
		# grab playlists
		p_lists = c.client.listplaylists()
		
		items = []
		for l in p_lists:
			desc = "%s (updated on %s)" % (l['playlist'], l['last-modified'])
			items.append(desc)
			l['desc'] = desc
		
		print(p_lists)
			
		msg = "Please select one of playlists from MPD's playlist directory:\n (current: %s)" % self.cfg_parser['mpd'].get('playlist_path')
		
		
		p_list, ok = QtWidgets.QInputDialog.getItem(self, "Choose MPD playlist", msg, items, 0, False)
		
		if ok:
			# determine name, because we're idiots and want to show playlist date
			p_list_name = None
			
			for l in p_lists:
				if l['desc'] == p_list:
					p_list_name = l['playlist']
			
			if p_list_name != None:
				print("[Playlist] loading %s" % p_list_name)
				c.client.load(p_list_name)
			else:
				print("[Playlist] Playlist %s not found", p_list)
		
	def stop(self):
		c = self.c
		s = self.s
		
		if not c.connected:
			c.connect()
			
		if s["state"] != "stop":
			c.client.stop()

	def dynamic(self, state):
		c = self.c
		s = self.s
		
		# switch 'Repopulate' button's state
		self.btnRepopulate.setEnabled(state)
		self.actionRepopulate.setEnabled(state)
		self.t.ToggleDynamic.emit(state)
		
		if state:
			# do this only if state = true!
			if self.npFilter.rowCount() <= self.cfg_parser['player'].getint('min'):
				print("[Dynamic] (playlist) nothing to play, repopulating...")
				self.t.RepopulatePlaylist.emit()
				c.fill_playlist()
			
			if s["state"] != "play" and self.npFilter.rowCount() > 0:	
				c.client.play(0)
			
		# save state
		if "ui_states" not in self.cfg_parser:
			self.cfg_parser["ui_states"] = {}
			oldState = False
		else:
			oldState = self.cfg_parser["ui_states"].getboolean("dynamic", False)
		
		if oldState != state:	
			self.cfg_parser["ui_states"]["dynamic"] = str(state)
			with open(self.config_path, 'w+') as configfile:
				self.cfg_parser.write(configfile)
			configfile.close()
		
	def volumeToggled(self, state):
		if "ui_states" not in self.cfg_parser:
			self.cfg_parser["ui_states"] = {}
			oldState = False
		else:
			oldState = self.cfg_parser["ui_states"].getboolean("volume", False)
		
		if oldState != state:	
			self.cfg_parser["ui_states"]["volume"] = str(state)
			with open(self.config_path, 'w+') as configfile:
				self.cfg_parser.write(configfile)
			configfile.close()
		
	def seekTrack(self, position):
		c = self.c
		
		if not c.connected:
			c.connect()
			
		# now, seek
		c.client.seekcur(position)
	
	def clearPlaylist(self):
		c = self.c
		
		c.client.clear()
		self.btnDynamicMode.setChecked(False)
		
		self.clearQueue()
	
	def repopulatePlaylist(self):
		c = self.c
		if self.cfg_parser['notifications'].getboolean('enabled'):
			self.notify("mpd: playlist", "Repopulating dynamic playlist")
		dyn = self.btnDynamicMode.isChecked()
		if dyn:
			self.t.RepopulatePlaylist.emit()
			c.client.clear()

	def updateVolume(self):
		os = self.s
		s = self.c.status()
		
		if s["volume"] != os["volume"]:
			volume = int(s["volume"])
			
			self.volumeBar.blockSignals(True)
			self.volumeBar.setValue(volume)
			self.volumeBar.blockSignals(False)
			
			self.s = s
	
	def setVolume(self):
		self.v = self.volumeBar.value()
		c = self.c
		s = self.s
		
		if int(s["volume"]) != self.v:
			if self.cfg_parser['notifications'].getboolean('volume_change'):
				v_msg = str("Volume: %02d%%" % self.v)
				if self.v == 0:
					icon = "audio-volume-muted"
				elif self.v > 0 and self.v <= 33:
					icon = "audio-volume-low"
				elif self.v > 33 and self.v < 66:
					icon = "audio-volume-medium"
				elif self.v >= 66:
					icon = "audio-volume-high"
					
				self.notify("mpd: volume", v_msg, icon)
			
			c.client.setvol(self.v)
	
	def setPlaybackOptionsTray(self):
		if self.cfg_parser['tray_icon'].getboolean('enabled'):
			c = self.c
			
			c.client.random(int(self.actionShuffle.isChecked()))
			#if self.actionShuffle.isChecked():
			#	self.tablePlaylist.showColumn(2)
			#else:
			#	self.tablePlaylist.hideColumn(2)
			c.client.repeat(int(self.actionRepeat.isChecked()))
			c.client.consume(int(self.actionConsume.isChecked()))
			c.client.single(int(self.actionSingle.isChecked()))
		
	
	def setPlaybackOptions(self):
		c = self.c

		c.client.random(int(self.btnShuffle.isChecked()))
		# if shuffle is active, show Queue column.
		#if self.btnShuffle.isChecked():
		#	self.tablePlaylist.showColumn(2)
		#else:
		#	self.tablePlaylist.hideColumn(2)
		
		c.client.repeat(int(self.btnRepeat.isChecked()))
		c.client.consume(int(self.btnConsume.isChecked()))
		c.client.single(int(self.btnSingle.isChecked()))
		
	def setPlaylist(self):
		c = self.c
		s = self.s
			
		data = c.current_playlist() # last row contains some statistics		
		# table header
		t_header = data[0]
		# table contents. w/o last row, it has some stats.
		cnt = len(data[1:])
		t_data = data[1:cnt]
		# total playtime for current playlist
		s_data = data[cnt]
		
		self.txtRuntime.setText(str(s_data["playtime"]))
		self.txtTracks.setText(str(s_data["tracks"]))
		
		#t_model = MyTableModel(t_data, t_header, self)
		t_model = QtGui.QStandardItemModel(cnt-1, len(t_header))
		
		z = 0
		for h in t_header:		
			t_model.setHeaderData(z, QtCore.Qt.Horizontal, h)
			z+=1
		
		i = 0
		for row in t_data:
			j = 0
			for column in t_header:
				txt = None
				if type(row[j]) == list:
					txt = row[j][0]
				else:
					txt = row[j]
					
				item = QtGui.QStandardItem(txt);
				t_model.setItem(i, j, item)
				j+=1
			i+=1
				
		self.npFilter.setSourceModel(t_model)
		self.npFilter.setFilterKeyColumn(-1)
		self.npFilter.setSortCaseSensitivity(0) # for some reason, python ignores this...	
		
		# set table data
		#self.tablePlaylist.setModel(self.npFilter)
		self.tablePlaylist.hideColumn(0)
		self.tablePlaylist.hideColumn(1)
		self.tablePlaylist.hideColumn(2)
		self.tablePlaylist.resizeColumnsToContents()
		
		# select active song in table
		#if s_data["tracks"] > 0 and s["state"] == "play":
		#	if not self.tablePlaylist.selectionModel().hasSelection():
		#		self.tablePlaylist.selectRow(int(c.now_playing()["pos"]))
	
	def filterPlaylist(self, txt):
		self.npFilter.setFilterFixedString(txt)
	
	def deleteFromPlaylist(self):
		c = self.c
		
		selection = self.tablePlaylist.selectionModel()
		
		if selection.hasSelection():
			items = selection.selectedRows()
			
			for item in items:
				r = item.row()
				
				songid = self.npFilter.index(r, 1).data()	
				c.client.deleteid(songid)
				print("[Playlist] removed track %s" % songid)
	
	stop_on_next = False
	def setStatus(self):
		c = self.c
		self.s = c.status()
		s = self.s
		# 'volume': '41', 'repeat': '1', 'random': '1', 'single': '0', 'consume': '1', 'playlist': '486', 'playlistlength': '14', 'mixrampdb': '0.000000', 'state': 'play', 'song': '13', 'songid': '285', 'time': '84:348', 'elapsed': '83.649', 'bitrate': '320', 'duration': '347.720', 'audio': '44100:24:2', 'nextsong': '2', 'nextsongid': '267'}
		
		# set volume
		vol = int(s["volume"])
		self.volumeBar.setValue(vol)
		
		# set repeat, random, consume etc.
		rep = bool(int(s["repeat"])) == True
		rand = bool(int(s["random"])) == True
		sng = bool(int(s["single"])) == True
		cns = bool(int(s["consume"])) == True
		
		# update buttons
		self.btnRepeat.setChecked(rep)
		self.btnShuffle.setChecked(rand)
		self.btnSingle.setChecked(sng)
		self.btnConsume.setChecked(cns)
		
		# update tray
		if self.cfg_parser['tray_icon'].getboolean('enabled'):
			self.actionRepeat.setChecked(rep)
			self.actionShuffle.setChecked(rand)
			self.actionSingle.setChecked(sng)
			self.actionConsume.setChecked(cns)
			
		state = s["state"]
				
		if self.stop_after != None:
			print(s['songid'], self.stop_after)
			if int(s['songid']) == self.stop_after or s['songid'] == self.stop_after:
				print("[Playback] Stopping playback after trackID %s" % self.stop_after)
				self.stop_on_next = True
		
		if self.stop_on_next and s['songid'] != self.stop_after:
			print("[Playback] Stopping playback, trackID %s finished" % self.stop_after)
			c.client.stop()
			self.stop_after = None
			self.stop_on_next = False
		
		if self.play_queue != None:
			if len(self.play_queue) > 0:
				# get first index
				track = self.play_queue[0]
				if s['songid'] != track['id']:
					# play it.
					if self.isTrackInPlaylist(track['id']):
						print("[Playback] playing trackID %s from queue" % track['id'])
						c.client.playid(track['id'])
					else:
						print("[Playback] TrackID %s not found in current playlist, removing from queue." % track['id'])
						self.play_queue.pop(0)
						self.updateQueueList(self.play_queue)
				else:
					# pop it from list
					self.play_queue.pop(0)
					self.updateQueueList(self.play_queue)
		
		# in case we're using queue method from shuffle. Also, think about the children :D
		if self.play_queue == None and len(self.model_data) > 0:
			print("[Playback] random mode queue detected, %d tracks" % len(self.model_data))
			t = self.model_data[0]
			
			if 'songid' in s:
				if s['songid'] == t['id']:
					self.model_data.pop(0)
					self.updateQueueList(self.model_data)
				
		np = c.now_playing()
		if state == "play":
			#np = c.now_playing()
			t = None
			np_not = np["artist"].replace("&", "&amp;")+" - "+np["track"].replace("&", "&amp;")+"\nfrom '"+np["album"].replace("&", "&amp;")+"'\nDuration: "+np["length"]
			
			if self.play_queue != None or len(self.model_data) > 0:
				if self.play_queue != None and len(self.play_queue) > 0:
					t = self.play_queue[0]
				elif len(self.model_data) > 0:
					t = self.model_data[0]
			elif 'nextsongid' in s:
				nxt = s['nextsongid']
				t = self.c.client.playlistid(nxt)[0]
			
			if t and len(t) > 0:
				title = "Unknown Title"
				artist = "Unknown Artist"
				album = "Unknown Album"
			
				if 'title' in t: title = t['title']
				if 'artist' in t: artist = t['artist']
				if 'album' in t: album = t['album']
			
				np_not +="\n\nNext: %s - %s" % (artist.replace("&", "&amp;"), title.replace("&", "&amp;"))
		
			self.set_status_text(np, s)
			
			tray_np = np["artist"]+" - "+np["track"]
			
			if self.cfg_parser['tray_icon'].getboolean('enabled'):
				self.actionPlaybackStatus.setText(tray_np if len(tray_np) < 50 else tray_np[:50]+"...")
				self.actionPlaybackStatus.setIcon(QtGui.QIcon.fromTheme("media-playback-start"))
			
			# send last.fm 'now playing'
			if self.last_fm != None:
				if self.lastfm_scrobble:
					self.last_fm.now_playing(np['artist'], np['album'], np['track'], np['seconds'])
				else:
					print("[last.fm] Not sending 'Now playing' status; scrobbling is paused.")
				if self.btnBanTrack.isChecked():
					# new track uncheck this thing.
					self.btnbanTrack.setChecked(False)
			
			if self.TrackTimer == None:
				self.TrackTimer = CountdownThread(s["songid"],  True)
				self.TrackTimer.signal.connect(self.update_timer)
				#self.TrackTimer.signal.connect(self.scrobblers)
				self.TrackTimer.start()
			
			# send notification
			if self.cfg_parser['notifications'].getboolean('now_playing'):
				self.notify("mpd: now playing", np_not, "media-playback-start")
			
			# set icon for now playing
			sIcon = QtGui.QIcon.fromTheme("media-playback-start").pixmap(24,24)
			self.icoNP.setPixmap(sIcon)
			
			# set icon and tooltip for 'Play' button
			self.btnToggle.setIcon(QtGui.QIcon.fromTheme("media-playback-pause"))
			self.btnToggle.setToolTip("Pause playback")
			
			if self.cfg_parser['tray_icon'].getboolean('enabled'):
				self.actionToggle.setIcon(QtGui.QIcon.fromTheme("media-playback-pause"))
				self.actionToggle.setText("Pause")
				self.actionToggle.setToolTip("Pause")
			
		elif state == "pause":
			#self.txtTrack.setText("MPD: paused playback")
			if self.cfg_parser['notifications'].getboolean('play_state'):
				self.notify("mpd: paused", "Playback paused", "media-playback-pause")
			
			# set icon for now playing
			sIcon = QtGui.QIcon.fromTheme("media-playback-pause").pixmap(24,24)
			self.icoNP.setPixmap(sIcon)
			
			# set icon and tooltip for 'Play' button
			self.btnToggle.setIcon(QtGui.QIcon.fromTheme("media-playback-start"))
			self.btnToggle.setToolTip("Resume playback")
						
			if self.cfg_parser['tray_icon'].getboolean('enabled'):
				self.actionToggle.setIcon(QtGui.QIcon.fromTheme("media-playback-start"))
				self.actionToggle.setText("Play")
				self.actionToggle.setToolTip("Play")
				
				self.actionPlaybackStatus.setIcon(QtGui.QIcon.fromTheme("media-playback-pause"))

		else:
			self.txtTrack.setText("MPD: stopped playback")
			if self.cfg_parser['notifications'].getboolean('play_state'):
				self.notify("mpd: stopped", "Playback stopped", "media-playback-stop")
						
			# set icon for now playing
			sIcon = QtGui.QIcon.fromTheme("media-playback-stop").pixmap(24,24)
			self.icoNP.setPixmap(sIcon)
			
			# set icon and tooltip for 'Play' button
			self.btnToggle.setIcon(QtGui.QIcon.fromTheme("media-playback-start"))
			self.btnToggle.setToolTip("Start playback")
			
			if self.cfg_parser['tray_icon'].getboolean('enabled'):
				self.actionToggle.setIcon(QtGui.QIcon.fromTheme("media-playback-start"))
				self.actionToggle.setText("Play")
				self.actionToggle.setToolTip("Play")
				self.actionPlaybackStatus.setIcon(QtGui.QIcon.fromTheme("media-playback-stop"))
			
			self.setWindowTitle(self.app['name'])
	
	def set_status_text(self, np=None, s=None):
		c = self.c
		
		if not c.connected:
			c.connect()
			
		if np == None:
			np = c.now_playing()
		
		if s == None:
			s = c.status()
		
		if s['state']  == "play":
			t = None
			
			np_txt = "<b>"+np["artist"]+" - "+np["track"]+"</b><br />from album '<i>"+np["album"]+"</i>'"
			
			
			if self.play_queue != None or len(self.model_data) > 0:
				if self.play_queue != None and len(self.play_queue) > 0:
					t = self.play_queue[0]
				elif len(self.model_data) > 0:
					t = self.model_data[0]
			elif 'nextsongid' in s:
				nxt = s['nextsongid']
				t = self.c.client.playlistid(nxt)[0]
			
			if t and len(t) > 0:
				title = "Unknown Title"
				artist = "Unknown Artist"
				album = "Unknown Album"
				if 'title' in t: title = t['title']
				if 'artist' in t: artist = t['artist']
				if 'album' in t: album = t['album']
				np_txt += "<br /><small>Next: <b>%s - %s</b> from album '<i>%s</i>'" % (artist, title, album)
			
			self.txtTrack.setText(np_txt)
		
	def secondsToTime(self, s):
		m, s = divmod(s, 60)
		h, m = divmod(m, 60)
		
		if h > 0:
			t = str("%02d:%02d:%02d" %(h,m, s))
		else:
			t = str("%02d:%02d" %(m, s))
			
		return t

	l_scrobbled = False
	lfm_scrobbled = False
	c_ab = 0
	def update_timer(self):
		c = self.c
		
		s = c.status()
		np = c.now_playing()
		
		if "elapsed" in s:
			el = int(round(float(s["elapsed"])))
		else:
			el = 0
		
		if "duration" in s:
			ln = int(round(float(s["duration"])))
		else:
			ln = 0
			
		elapsed = self.secondsToTime(el)
		total = self.secondsToTime(ln)
		pr_txt = str("%s / %s" % (elapsed, total))
		
		mX = self.progressSlider.maximum()
		if mX != ln:
			self.progressSlider.setMaximum(ln)
		
		self.progressSlider.setValue(el)
		self.txtTimer.setText(pr_txt)
		
		if s["state"] == "play":
			np_s = "Playing"
		elif s["state"] == "pause":
			np_s = "Paused"
		elif s["state"] == "stop":
			np_s = "Stopped"
		
		# update window title
		if np_s != None and np != None:
			title = "[MPD: %s] %s - %s [%s/%s] " %(np_s, np["artist"], np["track"], elapsed, total)
			self.setWindowTitle(title)
			self.tray.setToolTip(title)
	
		# call scrobblers on regular basis, but only if elapsed time's percentage can be divided by 10
		# this way, we're avoiding huge number of calls to last.fm functions, and still providing fair check period for local scrobbling.
		if el > 0 and ln > 0:
			el_percento = int(round((el/ln)*100))
		else:
			el_percento = 0
		
		a, b = divmod(el_percento, 10)
		
		if a > 0:
			if b in [0, 5, 10]:
				self.scrobblers()
		
	def scrobblers(self):
		c = self.c
		
		s = c.status()
		np = c.now_playing()
		
		if "elapsed" in s:
			el = int(round(float(s["elapsed"])))
		else:
			el = 0
		
		if "duration" in s:
			ln = int(round(float(s["duration"])))
		else:
			ln = 0
		
		# - local scrobbling/logging
		if self.l_Scrobbler != None:
			to_write = self.l_Scrobbler.check_percent(el, ln)
			if to_write:
				if not self.l_scrobbled:
					self.l_scrobbled = self.l_Scrobbler.scrobble(np["artist"], np["track"], np["album"], str(datetime.now()))
			else:
				if self.l_scrobbled:
					self.l_scrobbled = False	
		
		# - last.fm scrobbling/logging
		if self.last_fm != None and self.lastfm_scrobble:
			if not self.btnBanTrack.isChecked():
				to_scrobble = self.last_fm.check_percent(el, ln)
				if to_scrobble:
					if not self.lfm_scrobbled:
						self.lfm_scrobbled = self.last_fm.scrobble(artist=np["artist"], track=np["track"], album=np["album"],scrobble_time=None, dur=np["seconds"], sid=np["sid"])
				else:
					if self.lfm_scrobbled:
						self.lfm_scrobbled = False
			else:
				print("[last.fm] current track is banned, unban to scrobble")

class SystemTrayIcon(QtWidgets.QSystemTrayIcon):
	menu = None
	def __init__(self, icon, parent=None):
		QtWidgets.QSystemTrayIcon.__init__(self, icon, parent)
		self.menu = QtWidgets.QMenu(parent)
		self.setContextMenu(self.menu)
		self.setToolTip("myPyMPD") # TODO: set it dynamically.

def main():
	app = QtWidgets.QApplication(sys.argv)
	f = myPyMPD()
	f.show()
	app.exec_()

if __name__ == '__main__':
	main()
