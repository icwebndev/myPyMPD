# MPD controler library

from PyQt5 import Qt, QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QVariant, Qt, QSortFilterProxyModel, QObject, pyqtSignal
from mpd import (MPDClient, CommandError)
from random import choice
from socket import error as SocketError
from sys import exit
from daemonize import Daemonize
from select import select
from datetime import datetime
import threading
import logging
import time
import os
import signal
import sys
import notify2
import configparser

class MPDControler(QObject):
	# controler -> ui signals
	playerUpdate = pyqtSignal(name="PlayerUpdated")
	playlistUpdate = pyqtSignal(name="PlaylistUpdated")
	dbUpdateStarted = pyqtSignal(name="DatabaseUpdate")
	dbUpdateFinished = pyqtSignal(name="DatabaseUpdated")
	updateFinished = pyqtSignal(list, name="UpdateFinished")
	playbackOptsChanged = pyqtSignal(name="PlaybackUpdated")
	outputsChanged = pyqtSignal(name="OutputsChanged")
	volumeChanged = pyqtSignal(name="VolumeChanged")
	ControlerOperationNotification = pyqtSignal(int, int, bool, str)
	loopActivated = pyqtSignal(name="loopActivated")
	# ui -> controler signals
	dynamicModeChanged = pyqtSignal(bool, name="DynamicStarted")
	repopulatePlaylist = pyqtSignal(name="RepopulatePlaylist")
	configUpdated = pyqtSignal(configparser.ConfigParser, name="configUpdated")
	
	def __init__(self, cfg):
		super(MPDControler, self).__init__()
		self.system_running = False
		self.connected = False
		self.config = cfg['mpd']
		self.pl_config = cfg['player']
		self.pl_path = cfg['mpd']['playlist_path']
		self.pl_name = "Dynamic"
		saved_states = True if 'ui_states' in cfg else False
		if saved_states and cfg['ui_states'].getboolean('dynamic', False):
			self.pl_mode = "Dynamic"
		else:
			self.pl_mode = "Normal"
		self.pl_fill = False
		self.pl_repopulate = False
		self.client = MPDClient()
		self.s = None
	
	def update(self):
		if not self.connected:
			self.connect()
				
		# count how many times signal is sent
		counter = 0
		
		# send update request
		self.client.update()
				
		self.client.send_idle()
		
		while counter < 2:
			result = self.client.fetch_idle()
			
			if 'update' in result:
				counter+=1
			
			if counter < 2:	
				self.client.send_idle()
			else:
				self.updateFinished.emit(result)
				
	def connect(self):
		if not self.connected:
			try:
				self.client.connect(self.config["host"], self.config["port"])
				
				if self.config.getboolean('auth'):
					self.client.password(self.config["pswd"])

				self.connected = True
				return True
			except:
				err = str(sys.exc_info()[1])
				if err != "Already connected":
					print("[ERR] " +err)
					return False
		else:
			print("[MPD] Already connected")
	
	def test_connection(self, host="localhost", password=None, port=6600):
		try:
			self.client.connect(host, port)
			if password !=None:
				self.client.password(password)
			self.client.disconnect()
			return True
		except:
			print("[MPD] (test) connection error")
			return False
	
	def configUpdate(self, newConfig):
		print("[MPD] App configuration changed!")
		self.config = newConfig['mpd']
		self.pl_config = newConfig['player']
		self.pl_path = newConfig['mpd']['playlist_path']
		
	def toggleDynamic(self, state):		
		if state:
			print("[MPD] enabling dynamic mode")
			self.pl_mode = "Dynamic"
		else:
			print("[MPD] disabling dynamic mode")
			self.pl_mode = "Normal"
				
	def repopulate_playlist(self):
		self.pl_repopulate = True
		
	def current_playlist(self):	
		if not self.connected:
			self.connect()
		
		client = self.client
		pll = len(client.playlistinfo())
	
		pl = client.playlistinfo()			
		pl.insert(0, ["#", "SongID", "Queue", "Artist", "Track", "Album", "Length"])
		
		playtime = 0
		
		if pll > 0:		
			i = 0
			# format
			for x in pl:
				if i != 0:
					pos = int(x["pos"])+1
					if "album" in x and len(x["album"]) > 0:
						alb = x["album"]
					else:
						alb = "Unknown album"
					
					if "artist" in x and len(x["artist"]) > 0:
						art = x["artist"]
					else:
						art = "Unknown artist"
					
					if "title" in x and len(x["title"]) > 0:
						tit = x["title"]
					else:
						tit = "Unknown track"
					
					# calculate playtime
					playtime+=float(x["duration"])
					
					# now do that fancy h:m:s stuff
					m, s = divmod(float(x["duration"]), 60)
					h, m = divmod(m, 60)
					
					if h > 0:
						lng = str("%02d:%02d:%02d" % (h, m, s))
					else:
						lng = str("%02d:%02d" % (m, s))
					
					if "prio" in x:
						prio = x['prio']
					else:
						prio = 0
					
					pl[i] = [pos, x["id"], prio, art, tit, alb, lng]
				i+=1
		
		pm, ps = divmod(playtime, 60)
		ph, pm = divmod(pm, 60)
		pd, ph = divmod(ph, 24)
		
		pt = str("%d d, %d h, %d min, %d s" % (pd, ph, pm, ps))
		pl.insert(len(pl), {"playtime": pt, "tracks": len(pl[1:])})
		
		#client.close()
		return pl
	
	def status(self):
		if not self.connected:
			self.connect()
		
		client = self.client
		
		status = client.status()
		
		#client.close()
		return status
	
	def now_playing(self):
		if not self.connected:
			self.connect()
		
		client = self.client
		
		x = client.currentsong()
		if len(x) > 0:
			pos = int(x["pos"])+1
			if "album" in x and len(x["album"]) > 0:
				alb = x["album"]
			else:
				alb = "Unknown album"
						
			if "artist" in x and len(x["artist"]) > 0:
				art = x["artist"]
			else:
				art = "Unknown artist"
					
			if "title" in x and len(x["title"]) > 0:
				tit = x["title"]
			else:
				tit = "Unknown track"
			
			m, s = divmod(float(x["duration"]), 60)
			h, m = divmod(m, 60)
			
			if h > 0:
				lng = str("%d:%02d:%02d" % (h, m, s))
			else:
				lng = str("%02d:%02d" % (m, s))
			
			return {"artist": art, "album": alb, "track": tit, "length": lng, "seconds": x["time"], "pos": int(x["pos"]), "sid": int(x['id']) }
	
	def listener(self):
		if not self.connected:
			self.connect()
			
		c = self.client
		
		status = c.status()
		
		if "songid" in status:
			oldSID = status["songid"]
		else:
			oldSID = 0
		
		if "state" in status:
			oldState = status["state"]
		else:
			oldState = "stop"

		if "playlistlength" in status:
			oldPL = int(status["playlistlength"])
		else:
			oldPL = 0
			
		print("[MPD] (dbg) connecting to 'ui' signals")
		self.dynamicModeChanged.connect(self.toggleDynamic)
		self.repopulatePlaylist.connect(self.repopulate_playlist)
		self.configUpdated.connect(self.configUpdate)
		print("[MPD] (dbg) Sending 'loop active' signal")
		self.loopActivated.emit()
		
		print("[MPD] (dbg) Sending initial idle...")
		c.send_idle()
		
		self.system_running = True
		updNum = 0
		
		while self.system_running:
			changes = c.fetch_idle()
			status = c.status()
			self.s = status
			# mute this one, too, spams a lot
			#print("[MPD] (dbg) fetched idle: "+str(changes))
			
			if "player" in changes:
				if status["state"] == "play":
					if oldSID != status["songid"]:
						print("[MPD] (playback) track changed")
					elif oldState != status["state"]:
						print("[MPD] (playback) playback started")
					oldSID = status["songid"]
						
				elif status["state"] == "pause":
					print("[MPD] (playback) paused")
				elif status["state"] == "stop":
					print("[MPD] (playback) stopped")

				self.playerUpdate.emit()
				
				if "playlist" in changes:
					tracks = int(status["playlistlength"])
					if self.pl_mode == "Dynamic" and tracks <= int(self.pl_config["min"]):
						if status["state"] == "play" or self.pl_repopulate:
							print("[MPD] (%s) tracks left: %d; min: %s, refreshing" % (self.pl_mode, tracks, self.pl_config["min"]))
							self.fill_playlist()
						
						if self.pl_repopulate:
							c.play(0)
							self.pl_repopulate = False
						
						self.playlistUpdate.emit()
					elif self.pl_mode == "Dynamic" and tracks >= int(self.pl_config["min"]):
						self.playlistUpdate.emit()
					else:
						self.playlistUpdate.emit()
				
				oldState = status["state"]
				oldPL = int(status["playlistlength"])
			
			elif "playlist" in changes:
				tracks = int(status["playlistlength"])
				if self.pl_mode == "Dynamic":
					if not self.pl_repopulate and status["state"] == "play":
						if tracks <= int(self.pl_config["min"]):
							self.fill_playlist()
						
					self.playlistUpdate.emit()
						
				elif self.pl_mode != "Dynamic":
					if tracks != oldPL:
						self.playlistUpdate.emit()
					else:
						# just send the damn signal!
						self.playlistUpdate.emit()
					
			elif "update" in changes:
				if "database" in changes:
					stats = c.stats()
					txt = "\n\tLast update: "+datetime.fromtimestamp(int(stats["db_update"])).strftime("%x at %X")
					txt += "\n\tArtists: "+str(stats["artists"])
					txt += "\n\tAlbums: "+str(stats["albums"])
					txt += "\n\tSongs: "+str(stats["songs"])
					
					m, s = divmod(float(stats["db_playtime"]), 60)
					h, m = divmod(m, 60)
					d, h = divmod(h, 24)
					txt += "\n\tTotal playtime: "+ str("%d d, %02d h %02d min %02d s" % (d, h, m, s))
					updNum = 0
					print("[MPD] (database) Database updated:\n"+txt)
					
					self.dbUpdateFinished.emit()
				else:
					if updNum == 0:
						print("[MPD] (database) Database update started, please wait...")
						self.dbUpdateStarted.emit()
						updNum+=1
					elif updNum >= 1:
						print("[MPD] (database) Update finished, no changes found.")
						self.dbUpdateFinished.emit()
						updNum = 0
			
			elif "options" in changes:
				self.playbackOptsChanged.emit()
				print("[MPD] (options) Playback options updated")
			
			elif "output" in changes:
				outs = c.outputs()
				
				self.outputsChanged.emit()
				
				print("[MPD] (outputs) Outputs changed")
			
			elif "mixer" in changes:
				self.volumeChanged.emit()
				
				# mute this one, spams a lot
				#print("[INFO] Volume changed!")
				
			c.send_idle()
		
		return False

	def artistlist(self):
		if not self.connected:
			self.connect()
			
		c = self.client
		
		artists = c.list('artist')
		
		return artists
	
	def albumlist(self, artist=None):
		if not self.connected:
			self.connect()
			
		c = self.client
		
		if artist == None:
			albums = c.list('album')
		else:
			albums = c.list('album', 'artist', artist)
		
		return albums
		
	def tracklist(self, artist=None):
		if not self.connected:
			self.connect()
			
		c = self.client
		if artist == None:
			artists = c.list('artist')
			return_data = {}
		else:
			artists = [artist]	
			return_data = None

		for a in artists:
			tracks = c.find('any', a)
			if artist == None:
				return_data[a] = tracks
			else:
				return_data = tracks
			
		return return_data
	
	def add_random(self, trackCount):
		trackCount = int(trackCount)
		if not self.connected:
			self.connect()
			
		tracks = self.client.list('file')
		
		i = 1
		while i <= trackCount:
			track = choice(tracks)
			self.client.add(track)
			self.ControlerOperationNotification.emit(i, trackCount, True if i == trackCount else False, "Adding random tracks, %d of %d" % (i, trackCount))
			i+=1
		
	def feelingLucky_artist(self):
		if not self.connected:
			self.connect()
			
		artists = self.artistlist()
		
		chosen = choice(artists)
		
		tracks = self.tracklist(chosen)
		
		i = 1
		toAdd = len(tracks)
		
		for t in tracks:
			self.ControlerOperationNotification.emit(i, toAdd, True if i == toAdd else False, "I'm feeling lucky: artist")
			self.client.add(t['file'])
			i+=1

	def feelingLucky_album(self):
		if not self.connected:
			self.connect()
			
		albums = self.albumlist()
		
		album = choice(albums)
		
		tracks = self.client.list('file', 'album', album)
		
		i = 1
		toAdd = len(tracks)
		
		self.pl_fill = True
		for t in tracks:
			self.ControlerOperationNotification.emit(i, toAdd,  True if i == toAdd else False, "I'm feeling lucky: album")
			self.client.add(t)
			i+=1
	
	def addFiles(self, files):
		if not self.connected:
			self.connect()
				
		n = len(files)
		i = 1
		
		for f in files:
			self.ControlerOperationNotification.emit(i, n,  True if i == n else False, "Adding local files...")
			print("[MPD] (add_local) adding local file %s" % f)
			path = "file://%s" % f
			self.client.add(path)
			i+=1
		
	def fill_playlist(self):
		if not self.connected:
			self.connect()
		
		c = self.client
		
		self.pl_fill = True
		
		mpd_state = c.status()
		# odredi veličinu trenutne plejliste
		plSize = int(mpd_state["playlistlength"])
			
		# odredi broj pesama koji treba dodati
		toAdd = 0
		MAX_TRACKS = int(self.pl_config["max"])
		if plSize < MAX_TRACKS and plSize > 0:
			toAdd = MAX_TRACKS - plSize
		if plSize == 0:
			toAdd = MAX_TRACKS
		
		print("[MPD] (fill_playlist) Playlist has "+str(plSize)+"/"+str(MAX_TRACKS)+" tracks, adding "+str(toAdd)+" tracks...");
		if toAdd > 0:
			i = 1
			artists = self.artistlist()
			ignored_artists = []
			ignored_albums = []
			ignored_genres = []
			
			if len(self.pl_config['ignored_artists']) > 1:
				ignored_artists = self.pl_config['ignored_artists'].split(";")
			if len(self.pl_config['ignored_albums']) > 1:
				ignored_albums = self.pl_config['ignored_albums'].split(";")
			if len(self.pl_config['ignored_genres']) > 1:
				ignored_genres = self.pl_config['ignored_genres'].split(";")
			
			min_track_length = int(self.pl_config['min_length'])
			max_track_length = int(self.pl_config['max_length'])
			
			import re
			
			while i <= toAdd:
				# mark track as good from the start, and later do checks one by one and flag it properly
				good = True	
				
				# choose artist first, randomly
				art = choice(artists)
				if self.pl_config.getboolean('ignore_artists'):
					# now, check if artist is in "no-no" list
					for a in ignored_artists:
						regex_a = re.compile(".%s" % a, re.I)
						match_a = re.findall(regex_a, a)
						
						if len(match_a) > 0:
							good = False
				
				if good:
					# all ok, artist isn't banned, now you get a chance to check everything else.
					tracks = self.tracklist(art)
					if len(tracks) > 0:
						track = choice(tracks)
					else:
						good = False
				
				if self.pl_config.getboolean('ignore_albums') and good:
					# now check album
					if "album" in track:
						if type(track['album']) == list:
							album = track['album']
						else:
							album = [track['album']]
							
							for ig in ignored_albums:
								regex_b = re.compile("%s" % ig, re.I)
								for a in album:
									match_b = re.findall(regex_b, a)
									if len(match_b) > 0:
										good = False
				
				if self.pl_config.getboolean('ignore_genres') and good:
					# now check genre.
					if "genre" in track:
						if type(track['genre']) == list:
							genre = track['genre']
						else:
							genre = [track['genre']]
							
						for gn in ignored_genres:
							regex_c = re.compile(".%s" % gn, re.I)
							
							for g in genre:
								match_c = re.findall(regex_c, g)
								
								if match_c != None:
									good = False
				
				if self.pl_config.getboolean('ignore_duration') and good:
					# duration check is next...
					duration = int(track['time']) # we're comparing integers, I'm to lazy to enable minute/hour selector so everything is in seconds.
						
					if (min_track_length > 0 and duration < min_track_length) and (max_track_length > 0 and duration > max_track_length):
						good = False
						
				# finish all checks, if good, add		
				if good:
					c.add(track['file'])
					self.ControlerOperationNotification.emit(i, toAdd, self.pl_fill, "Dynamic playlist update, %d of %d" % (i, toAdd))
					i+=1
				
				# use this 'i' counter to remove pl_fill flag.
				if i == toAdd-1:
					self.pl_fill = False
					
		if mpd_state["state"] != "play":
			print("[DBG] Starting playback from fill_playlist()")
			c.play(0)

class LibraryUpdaterThread(QtCore.QThread):
	updateProgress = pyqtSignal(list, list, name="LibraryUpdateProgress") # first list [no_artists, current_artist]; second list [no_albums, current_album]
	updateFinished = pyqtSignal(list, list, dict, name="LibraryUpdateFinished")
	
	def __init__(self, cfg, parent=None):
		super(LibraryUpdaterThread, self).__init__(parent)
		self.cfg = cfg
		
	def run(self):
		if self.cfg != None:
			print("[MPD] Starting library scanner")
			c = MPDControler(self.cfg)
			c.connect()
			artists = c.artistlist()
			all_albums = c.albumlist()
			model_list = {}
			total = len(artists)
			a_counter = 1
			for artist in artists:
				if artist != "" and len(artist) > 1:
					artist_name = artist
				else:
					artist_name = "Unknown Artist"
				
				tracks = {}
				
				albums = c.albumlist(artist)
				
				total_albums = len(albums)
				
				al_counter = 0
				
				for album in albums:
					if album != "" and len(album) > 1:
						a = album
					else:
						a = "Unknown album"
					tracks[a] = c.client.find("Artist", artist, "Album", album)
					al_counter+=1
					
					# emit updateProgress signal
					self.updateProgress.emit([total, a_counter], [total_albums, al_counter])
					model_list[artist_name] = tracks
				a_counter+=1
			c.client.close()
			# finished, emit updateFinished
			self.updateFinished.emit(artists, all_albums, model_list )
			print("[MPD] Library scanner finished")
			
