# settings
from PyQt5 import Qt, QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QVariant, Qt, QSortFilterProxyModel, QObject, pyqtSignal
from mpd import (MPDClient, CommandError)
from random import choice
from socket import error as SocketError
from sys import exit
from daemonize import Daemonize
from select import select
from datetime import datetime, timedelta
import threading
import logging
import time
import os
import signal
import sys
import notify2
import configparser
import math
import json
import csv
import pylast
import xml.etree.ElementTree as ET
from mpd_controler import MPDControler

class LastFMScrobbler():
	def __init__(self, config, paths, test=False):
		self.config = config
		self.paths = paths
		self.test = test
		
		self.client = None
		
		# intialize client.
		if not self.test:
			self.get_client()

	def test_data(self, data):
		API_KEY = None
		API_SECRET = None
		USERNAME = None
		PASSWORD = None
		
		if 'api' in data:
			API_KEY = data['api']
		if 'secret' in data:
			API_SECRET = data['secret']
		if 'username' in data:
			USERNAME = data['username']
		if 'password' in data:
			PASSWORD = pylast.md5(data['password'])
		
		try:
			if API_KEY != None and API_SECRET != None and USERNAME != None and PASSWORD != None:
				pylast.LastFMNetwork(api_key=API_KEY, api_secret=API_SECRET, username=USERNAME, password_hash=PASSWORD)
				return True
			else:
				raise Exception("Missing last.fm login data")
		except:
			print("[last.fm] (test) %s: %s" % (sys.exc_info()[0], sys.exc_info()[1]))
			return False
		
	def get_client(self):
		"""
		instead of calling pylast.LastFMNetwork every time we need to connect, I've simply added this one to __init__
		
		Reads login data from configuration, checks if data exist, tries to login/auth with last.fm... or fails miserably.
		"""
		API_KEY = None
		API_SECRET = None
		USERNAME = None
		PASSWORD = None
		try:
			if 'api_key' in self.config and len(self.config.get('api_key')) > 0:
				API_KEY = self.config.get('api_key')
			
			if 'api_secret' in self.config and len(self.config.get('api_secret')) > 0:
				API_SECRET = self.config.get('api_secret')
			
			if 'username' in self.config and len(self.config.get('username')) > 0:
				USERNAME = self.config.get('username')
			
			if 'password' in self.config and len(self.config.get('password')) > 0:
				PASSWORD = pylast.md5(self.config.get('password'))
			
			if API_KEY != None and API_SECRET != None and USERNAME != None and PASSWORD != None:
				self.client = pylast.LastFMNetwork(api_key=API_KEY, api_secret=API_SECRET, username=USERNAME, password_hash=PASSWORD)
				return True
			else:
				raise Exception("Missing last.fm login data")
		except:
			print("[ERR] LastFMScrobbler: error creating Last.fm network client!")
			print("%s: %s" % (sys.exc_info()[0], sys.exc_info()[1]))
			return False

	def check_percent(self, c_time, t_time):
		"""
		this is simple function for an (even simpler) last.fm scrobble time check.
		
		variables:
		
		c_time -> current time
		t_time -> total time
		
		From last.fm API docs:
		A track should only be scrobbled when the following conditions have been met:

		- The track must be longer than 30 seconds.
		- And the track has been played for at least half its duration, or for 4 minutes (whichever occurs earlier.)
		
		So, we're checking first if:
		1. track is longer than 30 seconds - if it isnt, don't even test other variables
		2. is it on 50%
		3. if it isn't on 50%, is it's playtime more than 4 minutes long?
		"""
		# set False as default response
		scrobble = False
		c_perc = 50 # percentage when to scrobble
		c_mins = 240 # max number of seconds.
		# check if song is longer than 30 seconds:
		if t_time > 30:
			# calculate current playback percent
			if c_time > 0 and t_time > 0:
				p_perc = int(math.floor((c_time/t_time)*100))
			else:
				p_perc = 0
		
			# check if conditions meet
			if c_perc <= p_perc:
				# set scrobble flag to true
				scrobble = True
			else:
				if c_time >= c_mins:
					scrobble = True

		return scrobble
	
	def scrobble(self, artist=None, album=None, track=None, scrobble_time=None, dur=None, sid=None):
		"""
		main scrobbling function
		
		required data: artist, track (title)
		
		If album is not provided, doesn't scrobble it
		if scrobble_time is not provided, generates timestamp
		
		checks if artist/album/track is from 'unknown' category; doesn't scrobble if you don't have all data.
		"""
		try:
			# flag to scrobble. This one is true, unless proven differently.
			scrobble = True
			
			# check if artist is known
			if artist.lower() == "unknown artist" or artist.lower() == "unknown" or artist.lower() == "[unknown]" or artist.lower() == "" or artist == None:
				scrobble = False
				print("[DBG] LastFMScrobbler: missing artist name...")
				
			# check if track is unknown
			if track.lower() == "unknown track" or track.lower() == "unknown" or track.lower() == "[unknown]" or track.lower() == "" or track == None:
				scrobble = False
				print("[DBG] LastFMScrobbler: missing track name...")
			
			# inform if album is there
			if album == None or album.lower() == "unknown" or album.lower() == "unknown album":
				print("[DBG] LastFMScrobbler: ignoring album - not given or unknown")
			
			# set scrobble time if not given
			if scrobble_time == None:
				scrobble_time = datetime.utcnow() - datetime(1970,1,1)
#				scrobble_time = int(time.mktime(datetime.now().timetuple())) # unixtime, maaan...
			
			# check if track is scrobbled recently. Function returns true/false, so no if clause is necessary.
			# have to go this way, because client should return True, not false because no matter what, it's a success and we don't want a log flood.
			is_scrobbled = self.check_if_scrobbled(artist, track, album, dur, scrobble_time, sid)
			
			# change it's value ONLY if it is true, since this is last check step.
			if scrobble:
				scrobble = is_scrobbled
			
			if scrobble:
				print("[last.fm] track to be scrobbled")
				
			# OK, now, scrobble time?
			response = "Not scrobbling";
			if self.client != None and scrobble:
				if album != None:
					response = self.client.scrobble(artist, track, scrobble_time.total_seconds(), album)
				else:
					response = self.client.scrobble(artist, track, scrobble_time.total_seconds())
				
				self.log_scrobbled(artist, album, track, scrobble_time.total_seconds(), sid)
				
				print("[last.fm] reponse: %s" % str(response))
				print("[last.fm] Scrobbled %s by %s" % (track, artist))
				return True
			else:
				if self.client == None:
					raise Exception("No connection to last.fm or something wrong with last.fm data. Check response.")
					return False
				elif not is_scrobbled:
					return True
				elif not scrobble and is_scrobbled:
					raise Exception("Problem scrobbling track, probably bad or incomplete data.")
					return False
				else:
					print(is_scrobbled, scrobble)
					raise Exception("Unknown error. Check log")
					return False
		except:
			print("[ERR] LastFMScrobbler: error!")
			print("%s: %s" % (sys.exc_info()[0], sys.exc_info()[1]))
			return False
		
	def log_scrobbled(self, artist, album, track, timestamp, sid):
		prev_data = None
				
		# generate object
		s_entry = {"timestamp":timestamp,"artist":artist,"album":album,"title":track,"sid":sid}
		
		# create directory if it doesn't exist
		if not os.path.exists("%s/last.fm/" % self.paths.get('storage')):
			os.mkdir("%s/last.fm/" % self.paths.get('storage'))
		
		# generate path, we're gonna call this thing many times
		l_file = "%s/last.fm/scrobble_log.json" % self.paths.get('storage')
		
		# if log file doesn't, exist, prev_data is empty.
		if not os.path.exists(l_file):
			prev_data = []
		else:
			# log file exists, read it and give data a chance :D
			with open(l_file, 'r') as json_log:
				prev_data = json.load(json_log)
				json_log.close()
						
		if prev_data != None:
			prev_data.append(s_entry)
				
			with open(l_file, 'w') as json_log:
				json_log.write(json.dumps(prev_data, indent=4))
				json_log.close()
	
	def check_if_scrobbled(self, artist, track, album, dur, timestamp, sid):
		"""
		This one should check if track is actually scrobled not too long ago
		
		usage case:
			let's say, track gets scrobbled. You close the player, or rewind it 30-40 seconds back from scrobble point.
			Once you start player again (case a) while track is playing or check function hits 50% mark again,
			track will get scrobbled again
		
		main reason why I'm logging scrobbles - to be able to check if time between two scrobbles is actually
		bigger than at least 80% of song's length, to prevent repeat cheating.
		
		Called from 'scrobble()' because check_percent doesn't provide any of requested data - it's a simple comparation f-ion.
		"""
		# scrobbable flag
		scrobble = True
		
		# generate path, we're gonna call this thing many times
		l_file = "%s/last.fm/scrobble_log.json" % self.paths.get('storage')
		
		# bail if file doesn't exist
		if not os.path.exists(l_file):
			print("[last.fm] scrobble log doesn't exist, not checking if tracks is already scrobbled")
		else:
			print("[last.fm] making sure track isn't scrobbled too soon")
			# log file exists, read it and give data a chance :D
			with open(l_file, 'r') as json_log:
				data = json.load(json_log)
				#json_log.close()
				print("[last.fm] scrobbling log loaded, contains %d records" % len(data))
			
			t_count = len(data)
			t_min = t_count-5
			t_max = t_count
			
			# check just last 10 tracks
			# why? if average track is 4 minutes long, 5 tracks x 4 minutes = 20 minutes
			# So yeah, no need to go through bunch of records
			print("[last.fm] searching for %s - %s (ID: %d) in log" % (artist, track, sid))
			for d in data[t_min:t_max]:
				# check if metadata is good.
				# had to hund via SID, for some reason string comparison isn't a way it works well...
				#print("[last.fm] checking %s - %s (%s)" % (d['artist'], d['title'], d['sid']))
				if int(d["sid"]) == int(sid):
					print("[last.fm] found track in scrobbled list, checkup started")
					d_timestamp = d['timestamp'] # track's timestamp
					d_dur = int(dur)
					d_proc = (d_dur*(80/100)) # how long is actually 80% of a track
					d_50proc = (d_dur*(50/100))
					
					c_timestamp = (datetime.utcnow() - datetime(1970,1,1)).total_seconds()

					then = datetime.utcfromtimestamp(d_timestamp)
					now = datetime.utcfromtimestamp(c_timestamp)
					
					print("[last.fm] previous scrobble was at %s, and now is %s" % (then, now))
					delta = now - then
					
					print("[last.fm] (dbg) timedelta: %f" % delta.total_seconds())
					msg = None
					
					m, s = divmod(delta.total_seconds(), 60)
					m1, s1 = divmod(d_proc, 60)
					m2, s2 = divmod(d_dur, 60)
					m3, s3 = divmod(d_50proc, 60)
					# if 50% is less than 4 minutes, and difference between scrobbles is less or equal than  80% of track's duration
					# why 80? let's go this way:
					# - track is played
					# - track is scrobbled at 50%
					# - you finish the track (+50% from scrobble point)
					# - track repeats
					# - next scrobble point is at exactly 100% from previous one.
					# This function tolerates up to 20% difference between two scrobbles. if you really need more...
					if d_50proc <= 240 and float(delta.total_seconds()) <= d_proc:
						msg = "[last.fm] previous scrobble was %02d:%02d ago - less than %02d:%02d (80%% of duration) , ignoring." % (m, s, m1, s1)
						scrobble = False
					# now, if 50% is more than 4 minutes and difference between scrobbles is less than 4 minutes...
					elif d_50proc > 240 and delta.total_seconds() <= d_proc:
						msg = "[last.fm] previous scrobble was %02d:%02d ago, track is %02d:%02d long.. Ignoring." % (m, s, m2, s2)
						scrobble = False
					elif delta.total_seconds() > d_proc:
						msg = "[last.fm] previous scrobble was %02d:%02d ago. " % (m, s)
						if d_50proc > 240:
							msg += "Track's 50%% is at %02d:%02d, which is more than 4 mins. Scrobbling" % (m3, s3)
						else:
							msg += "Track's 50%% is at %02d:%02d, scrobbling" %(m3, s3)
					# inform user
					print(msg)
			
			if scrobble:
				print("[last.fm] track not found in last %d tracks, scrobbling" % (t_max-t_min))
		# return flag
		return scrobble
		
	def now_playing(self, artist=None, album=None, track=None, dur=None):
		# flag to scrobble. This one is true, unless proven differently.
		scrobble = True
		
		# check if artist is known
		if artist.lower() == "unknown artist" or artist.lower() == "unknown" or artist.lower() == "[unknown]" or artist.lower() == "" or artist == None:
			scrobble = False
			print("[last.fm] missing artist name, ignoring track")
			
		# check if track is unknown
		if track.lower() == "unknown track" or track.lower() == "unknown" or track.lower() == "[unknown]" or track.lower() == "" or track == None:
			scrobble = False
			print("[last.fm] missing track title, ignoring track")
		
		# inform if album is there
		if album == None or album.lower() == "unknown" or album.lower() == "unknown album":
			print("[last.fm] unknown album, or album not provided. not sending 'album' tag")
		
		# OK, now, scrobble time?
		if self.client != None and scrobble:
			if dur == None:
				self.client.update_now_playing(artist, track, album)
			else:
				self.client.update_now_playing(artist, track, album, duration=dur)
		else:
			if self.client == None:
				print("[last.fm] can't scrobble, client isn't configured")
			elif not scrobble:
				print("[last.fm] can't scrobble, track is ignored (bad metadata)")
	
	def love_track(self, artist=None, track=None):
		love = True
		
			# check if artist is known
		if artist.lower() == "unknown artist" or artist.lower() == "unknown" or artist.lower() == "[unknown]" or artist.lower() == "" or artist == None:
			scrobble = False
			print("[last.fm] missing artist name, ignoring track")
			
		# check if track is unknown
		if track.lower() == "unknown track" or track.lower() == "unknown" or track.lower() == "[unknown]" or track.lower() == "" or track == None:
			scrobble = False
			print("[last.fm] missing track title, ignoring track")
		
		# OK, now, scrobble time?
		if self.client != None and love:
			print("[last.fm] Loved %s by %s" % (track, artist))
			song = pylast.Track(artist, track, self.client)
			song.love()
		else:
			if self.client == None:
				print("[last.fm] can't love, client isn't configured")
			elif not scrobble:
				print("[last.fm] can't love, track is ignored (bad metadata)")
		
class LocalScrobbler():
	def __init__(self, config):
		self.config = config
		
	def check_percent(self, c_time, t_time):
		# configured percent
		c_perc = self.config.getint('perc_played')
		
		# current playback percent
		if c_time > 0 and t_time > 0:
			p_perc = int(math.floor((c_time/t_time)*100))
		else:
			p_perc = 0
		
		# check if conditions meet
		if c_perc <= p_perc:
			return True
		else:
			return False
	
	def scrobble(self, artist, title, album, time):
		method = self.config.get('format')
		succ = False
		
		if method == "JSON":
			try:
				old_data = None
				
				# generate object
				s_entry = {"time":time,"artist":artist,"album":album,"title":title}
				
				if not os.path.exists(self.config.get('path')):
					#with open(self.config.get('path'), 'w') as json_log:
					#	json.dump([], indent=4)
					#	json_log.close()
					old_data = []
				else:
					with open(self.config.get('path'), 'r') as json_log:
						old_data = json.load(json_log)
						json_log.close()
						
				if old_data != None:
					old_data.append(s_entry)
					
					with open(self.config.get('path'), 'w') as json_log:
						json_log.write(json.dumps(old_data, indent=4))
						json_log.close()
				
				succ = True
			except:
				print("[LocalScrobbler | %s] %s: %s" % (method, sys.exc_info()[0], sys.exc_info()[1]))
		
		elif method == "XML":
			try:
				old_data = None
				tree = None
				
				if os.path.exists(self.config.get('path')):
					tree = ET.parse(self.config.get('path'))
					old_data = tree.getroot()
				else:
					old_data = ET.Element("data")
								
				# create new entry
				new_entry = ET.SubElement(old_data, "track")
				# add time
				sub_time = ET.SubElement(new_entry, "time")
				sub_time.text = time
				# add artist
				sub_artist = ET.SubElement(new_entry, "artist")
				sub_artist.text = artist
				# add album
				sub_album = ET.SubElement(new_entry, "album")
				sub_album.text = album
				# add title
				sub_title = ET.SubElement(new_entry, "title")
				sub_title.text = title

				if tree != None:
					tree.write(self.config.get('path'))
				elif tree == None:
					ET.ElementTree(old_data).write(self.config.get('path'))
				
				succ = True
			except:
				print("[LocalScrobbler | %s] %s: %s" % (method, sys.exc_info()[0], sys.exc_info()[1]))
			
		elif method == "Text":
			try:
				l_entry = "Time: %s; Artist: %s; Album: %s; Title: %s;\n" % (time, artist, album, title)
				with open(self.config.get('path'), 'a') as p_log:
					p_log.write(l_entry)
					p_log.close()
				
				succ = True
			except:
				print("[LocalScrobbler | %s] %s: %s" % (method, sys.exc_info()[0], sys.exc_info()[1]))
		
		elif method == "CSV":
			try:
				new_table = False
				l_header = ("Time played", "Artist", "Album", "Title")
				
				l_entry = (time, artist, album, title)
				
				if not os.path.exists(self.config.get('path')):
					new_table = True
				
				with open(self.config.get('path'), 'a') as csv_log:
					csv_writer = csv.writer(csv_log, delimiter=";", quotechar='"', quoting=csv.QUOTE_ALL)
					if new_table:
						csv_writer.writerow(l_header)
					
					csv_writer.writerow(l_entry)
					csv_log.close()
					
				succ = True
			except:
				print("[LocalScrobbler | %s] %s: %s" % (method, sys.exc_info()[0], sys.exc_info()[1]))
		
		if succ:
			print("[LocalScrobbler | %s] [%s] %s by %s (%s) logged to %s" % (method, time, title, artist, album, self.config.get('path')))
		
		return succ